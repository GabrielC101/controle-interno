# Controle Interno | Internal Control

Projeto empresarial desenvolvido com fins no controle interno de máquinas e rotinas diarias.
Conta com as tecnologias Spring Boot no back-end e Angular no front-end.

Business project developed for internal control of machines and daily routines.
It has Spring Boot technologies on the back end and Angular on the front end.

> Tecnologias | Technology

- Spring Boot
- Angular 7
- SQL Server

> API's

- JWT
- Spring Security
- JavaMail
- Micro Services
- Estrutura REST
- JPA
- Hibernate
- ngx-mask
- ngx-pdf
- ngx-toastr
- ng2-img-max
- Archive Upload
- ngx-qrcode
- ngx-pagination

> Módulos | Modules

 - Patrimônio
 - Contas a Pagar
 - Notas Fiscais
 - Empresas
 - Backups
 - Certificados
 - Domínio
 - E-mail Malote
 - Usuários
 

> Registro

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/login-register/register.png">

> Login

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/login-register/login.png">

> Dashboard

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/dashboard/dashboard.png">

> Perfil

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/perfil/perfil.png">

- Alteração de Senha

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/perfil/senha.png">

> Patrimônio

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/patrimonio/menu.png">

- Inserir

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/patrimonio/alteracao.png">

- Listar

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/patrimonio/selecao.png">

- Manutenção

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/patrimonio/manutencao.png">

- Cadastro de Manutenção

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/patrimonio/cadastro-manutencao.png">

- Gerador de PDF

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/patrimonio/pdf.png">

> Contas a Pagar

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/contas-a-pagar/listar.png">

- Inserir

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/contas-a-pagar/contas.png">

- Alterar | Geração de QRCode para o link do Site.

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/contas-a-pagar/alterar.png">

> Notas Fiscais

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/nfe/listar.png">

- Inserir

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/nfe/cadastro.png">

> Empresa e Departamento

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/cadastro-empresa-dep/empresa-dep.png">

> Backups

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/backups/lista.png">

- Inserir

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/backups/cadastro.png">

> Certificados

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/certificados/lista.png">

- Inserir

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/certificados/cadastro.png">

> Domínios

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/dominios/lista.png">

- Inserir

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/dominios/cadastro.png">

> Usuários

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/email/configuracoes.png">

- Menu

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/email/menu.png">

- Grupos

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/email/grupos.png">

- Lista

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/email/lista.png">

- Contatos

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/email/contatos.png">

- Transmitir

<img src="https://gitlab.com/GabrielC101/controle-interno/-/raw/master/frontend-angular/src/assets/prototipos/email/transmitir.png">

