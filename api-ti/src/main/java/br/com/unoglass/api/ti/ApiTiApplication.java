package br.com.unoglass.api.ti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiTiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiTiApplication.class, args);
	}

}
