package br.com.unoglass.api.ti.arquivo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.unoglass.api.ti.arquivo.model.Arquivo;
import br.com.unoglass.api.ti.arquivo.service.ArquivoService;

@CrossOrigin
@RestController
@RequestMapping(value="/api/ti/arquivo")
public class ArquivoController {

	@Autowired
	ArquivoService arquivoService;
	
	@CrossOrigin
	@PostMapping(value="/empresa/{id}/texto/{string}", consumes = "multipart/form-data")
	public Arquivo salvar(@PathVariable(name="id") Long id, @PathVariable(name="string") String text, 
			@RequestParam("arquivo") MultipartFile file) {
		Arquivo arquivo = new Arquivo();
		try {			
			arquivo.setContentType(file.getContentType());
			arquivo.setNomeArquivo(file.getOriginalFilename());
			arquivo.setConteudoArquivo(file.getBytes());
			arquivo.setGuidEmpresa(id);
			arquivo.setDescricaoArquivo(text);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return arquivoService.save(arquivo);
	}
	
	@GetMapping("/{guidArquivo}")
	public Arquivo findOneById(@PathVariable("guidArquivo") Long guidArquivo) {
		return arquivoService.findOneById(guidArquivo);
	}
	
	@GetMapping("/download/{guidArquivo}")
	public ResponseEntity<Resource> download(@PathVariable("guidArquivo") Long guidArquivo) {
		Arquivo arquivo = arquivoService.findOneById(guidArquivo);
		Resource resource = new ByteArrayResource(arquivo.getConteudoArquivo());
		String contentType = arquivo.getContentType();
		if (contentType == null) {
			contentType = "application/octet-stream";
		}
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + 
						arquivo.getNomeArquivo() + "\"")
				.body(resource);
	}
	
}
