package br.com.unoglass.api.ti.arquivo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="ARQUIVOS")
public class Arquivo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long guidArquivo;
	private String nomeArquivo;
	@Lob
	private byte[] conteudoArquivo;
	private String contentType;
	private Long guidRecado;
	private Long guidEmpresa;
	private String descricaoArquivo;
	
	public String getDescricaoArquivo() {
		return descricaoArquivo;
	}
	public void setDescricaoArquivo(String descricaoArquivo) {
		this.descricaoArquivo = descricaoArquivo;
	}
	public Long getGuidEmpresa() {
		return guidEmpresa;
	}
	public void setGuidEmpresa(Long guidEmpresa) {
		this.guidEmpresa = guidEmpresa;
	}
	public Long getGuidRecado() {
		return guidRecado;
	}
	public void setGuidRecado(Long guidRecado) {
		this.guidRecado = guidRecado;
	}
	public Long getGuidArquivo() {
		return guidArquivo;
	}
	public void setGuidArquivo(Long guidArquivo) {
		this.guidArquivo = guidArquivo;
	}
	public String getNomeArquivo() {
		return nomeArquivo;
	}
	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
	public byte[] getConteudoArquivo() {
		return conteudoArquivo;
	}
	public void setConteudoArquivo(byte[] conteudoArquivo) {
		this.conteudoArquivo = conteudoArquivo;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
}
