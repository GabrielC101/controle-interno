package br.com.unoglass.api.ti.arquivo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.unoglass.api.ti.arquivo.model.Arquivo;

public interface ArquivoRepository extends JpaRepository<Arquivo, Long> {

}
