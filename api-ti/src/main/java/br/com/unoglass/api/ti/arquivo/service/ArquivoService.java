package br.com.unoglass.api.ti.arquivo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.arquivo.model.Arquivo;
import br.com.unoglass.api.ti.arquivo.repository.ArquivoRepository;

@Service
public class ArquivoService {

	@Autowired
	ArquivoRepository arquivoRepository;
	
	public Arquivo findOneById(Long guidArquivo) {
		return arquivoRepository.findById(guidArquivo).get();
	}

	public Arquivo save(Arquivo arquivo) {
		return arquivoRepository.save(arquivo);
	}

}
