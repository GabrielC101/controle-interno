package br.com.unoglass.api.ti.backup.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unoglass.api.ti.backup.model.Backup;
import br.com.unoglass.api.ti.backup.service.BackupService;

@RestController
@RequestMapping(value="/api/ti/backup")
public class BackupController {

	@Autowired
	BackupService backupService;
	
	@GetMapping
	public List<Backup> listAll() {
		return backupService.listarTodos();
	}
	
	@GetMapping(value="/empresa/{id}")
	public List<Backup> listByEmpresa(@PathVariable Long id) {
		return backupService.listByEmpresa(id);
	}
	
	@GetMapping(value="/{id}")
	public Optional<Backup> listOne(@PathVariable Long id) {
		return backupService.listarUm(id);
	}
	
	@PostMapping
	public Backup insertOne(@RequestBody Backup backup) {
		return backupService.inserirUm(backup);
	}
	
	@DeleteMapping(value="/{id}")
	public void deleteOne(@PathVariable Long id) {
		backupService.deletarUm(id);
	}
	
}
