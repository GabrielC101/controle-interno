package br.com.unoglass.api.ti.backup.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BACKUPS")
public class Backup {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long guidBackup;
	private String nomeBackup;
	private Long guidEmpresa;
	private String caminhoBackup;
	private String softwareNome;
	private Long guidSoftware;
	private String usuarioRede;
	private String senhaRede;
	
	public Long getGuidSoftware() {
		return guidSoftware;
	}
	public void setGuidSoftware(Long guidSoftware) {
		this.guidSoftware = guidSoftware;
	}
	public String getUsuarioRede() {
		return usuarioRede;
	}
	public void setUsuarioRede(String usuarioRede) {
		this.usuarioRede = usuarioRede;
	}
	public String getSenhaRede() {
		return senhaRede;
	}
	public void setSenhaRede(String senhaRede) {
		this.senhaRede = senhaRede;
	}
	public Long getGuidBackup() {
		return guidBackup;
	}
	public void setGuidBackup(Long guidBackup) {
		this.guidBackup = guidBackup;
	}
	public String getNomeBackup() {
		return nomeBackup;
	}
	public void setNomeBackup(String nomeBackup) {
		this.nomeBackup = nomeBackup;
	}
	public Long getGuidEmpresa() {
		return guidEmpresa;
	}
	public void setGuidEmpresa(Long guidEmpresa) {
		this.guidEmpresa = guidEmpresa;
	}
	public String getCaminhoBackup() {
		return caminhoBackup;
	}
	public void setCaminhoBackup(String caminhoBackup) {
		this.caminhoBackup = caminhoBackup;
	}
	public String getSoftwareNome() {
		return softwareNome;
	}
	public void setSoftwareNome(String softwareNome) {
		this.softwareNome = softwareNome;
	}
	
}
