package br.com.unoglass.api.ti.backup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.unoglass.api.ti.backup.model.Backup;

public interface BackupRepository extends JpaRepository<Backup, Long>{

	@Query("select b from Backup b where b.guidEmpresa = ?1")
	List<Backup> findByEmpresa(Long id);

}
