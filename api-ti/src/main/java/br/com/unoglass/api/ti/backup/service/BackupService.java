package br.com.unoglass.api.ti.backup.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.backup.model.Backup;
import br.com.unoglass.api.ti.backup.repository.BackupRepository;
import br.com.unoglass.api.ti.util.BusinessException;
import br.com.unoglass.api.ti.util.BusinessExceptionCode;

@Service
public class BackupService {

	@Autowired
	BackupRepository backupRepository;
	
	public List<Backup> listarTodos() {
		try {
			return backupRepository.findAll();
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR015);
		}
	}
	
	public Optional<Backup> listarUm(Long id) {
		try {
			return backupRepository.findById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR015);
		}
	}
	
	public Backup inserirUm(Backup backup) {
		try {
			return backupRepository.save(backup);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR016);
		}
	}
	
	public void deletarUm(Long id) {
		try {
			backupRepository.deleteById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR008);
		}
	}

	public List<Backup> listByEmpresa(Long id) {
		try {
			return backupRepository.findByEmpresa(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR015);
		}
	}
	
}
