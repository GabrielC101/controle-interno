package br.com.unoglass.api.ti.certificados.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.unoglass.api.ti.certificados.model.Certificado;
import br.com.unoglass.api.ti.certificados.service.CertificadoService;

@RestController
@RequestMapping(value="/api/ti/certificado")
public class CertificadoController {

	@Autowired
	CertificadoService certificadoService;
	
	@CrossOrigin
	@PostMapping(value="/empresa/{id}/password/{string}/name/{name}", consumes = "multipart/form-data")
	public Certificado salvar(@PathVariable(name="id") Long id, @PathVariable(name="string") String text, 
			@PathVariable(name="name") String name, @RequestParam("arquivo") MultipartFile file) {
		Certificado arquivo = new Certificado();
		try {			
			arquivo.setContentType(file.getContentType());
			arquivo.setNomeArquivo(file.getOriginalFilename());
			arquivo.setConteudoArquivo(file.getBytes());
			arquivo.setSenhaCertificado(text);
			arquivo.setNomeCertificado(name);
			arquivo.setGuidEmpresa(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return certificadoService.save(arquivo);
	}
	
	@GetMapping("/{guidArquivo}")
	public Certificado findOneById(@PathVariable("guidArquivo") Long guidArquivo) {
		return certificadoService.findOneById(guidArquivo);
	}
	
	@GetMapping("/download/{guidArquivo}")
	public ResponseEntity<Resource> download(@PathVariable("guidArquivo") Long guidArquivo) {
		Certificado arquivo = certificadoService.findOneById(guidArquivo);
		Resource resource = new ByteArrayResource(arquivo.getConteudoArquivo());
		String contentType = arquivo.getContentType();
		if (contentType == null) {
			contentType = "application/octet-stream";
		}
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + 
						arquivo.getNomeArquivo() + "\"")
				.body(resource);
	}
	
	@GetMapping(value="/empresa/{id}")
	public List<Certificado> listCertificados(@PathVariable Long id) {
		return certificadoService.listarByEmpresa(id);
	}
	
	@DeleteMapping(value="/{id}")
	public void deleteCertificado(@PathVariable Long id) {
		certificadoService.deleteById(id);
	}
	
}
