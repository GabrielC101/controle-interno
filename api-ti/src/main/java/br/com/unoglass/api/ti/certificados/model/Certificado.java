package br.com.unoglass.api.ti.certificados.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="CERTIFICADO")
public class Certificado {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long guidCertificado;
	private String nomeArquivo;
	@Lob
	private byte[] conteudoArquivo;
	private String contentType;
	private String senhaCertificado;
	private String nomeCertificado;
	private Long guidEmpresa;
	
	public Long getGuidEmpresa() {
		return guidEmpresa;
	}
	public void setGuidEmpresa(Long guidEmpresa) {
		this.guidEmpresa = guidEmpresa;
	}
	public Long getGuidCertificado() {
		return guidCertificado;
	}
	public void setGuidCertificado(Long guidCertificado) {
		this.guidCertificado = guidCertificado;
	}
	public String getNomeArquivo() {
		return nomeArquivo;
	}
	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
	public byte[] getConteudoArquivo() {
		return conteudoArquivo;
	}
	public void setConteudoArquivo(byte[] conteudoArquivo) {
		this.conteudoArquivo = conteudoArquivo;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getSenhaCertificado() {
		return senhaCertificado;
	}
	public void setSenhaCertificado(String senhaCertificado) {
		this.senhaCertificado = senhaCertificado;
	}
	public String getNomeCertificado() {
		return nomeCertificado;
	}
	public void setNomeCertificado(String nomeCertificado) {
		this.nomeCertificado = nomeCertificado;
	}
	
}
