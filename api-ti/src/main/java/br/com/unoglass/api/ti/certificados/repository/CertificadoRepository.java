package br.com.unoglass.api.ti.certificados.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.unoglass.api.ti.certificados.model.Certificado;

public interface CertificadoRepository extends JpaRepository<Certificado, Long>{

	@Query("select c from Certificado c where c.guidEmpresa = ?1")
	List<Certificado> findByEmpresa(Long id);

}
