package br.com.unoglass.api.ti.certificados.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.certificados.model.Certificado;
import br.com.unoglass.api.ti.certificados.repository.CertificadoRepository;

@Service
public class CertificadoService {

	@Autowired
	CertificadoRepository certificadoRepository;
	
	public Certificado findOneById(Long guidArquivo) {
		return certificadoRepository.findById(guidArquivo).get();
	}

	public Certificado save(Certificado arquivo) {
		return certificadoRepository.save(arquivo);
	}
	
	public List<Certificado> listarByEmpresa(Long id) {
		return certificadoRepository.findByEmpresa(id);
	}

	public void deleteById(Long id) {
		certificadoRepository.deleteById(id);
	}
}
