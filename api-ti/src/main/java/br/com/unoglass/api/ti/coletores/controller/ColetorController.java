package br.com.unoglass.api.ti.coletores.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unoglass.api.ti.coletores.model.Coletor;
import br.com.unoglass.api.ti.coletores.service.ColetorService;

@RestController
@RequestMapping(value="/api/ti/coletores")
public class ColetorController {
	
	@Autowired
	ColetorService coletorService;
	
	@GetMapping
	public List<Coletor> listAll() {
		return coletorService.listarTodos();
	}
	
	@GetMapping(value="/{id}")
	public Optional<Coletor> listOne(@PathVariable Long id) {
		return coletorService.listarUm(id);
	}
	
	@PostMapping
	public Coletor insertOne(@RequestBody Coletor coletor) {
		return coletorService.inserirUm(coletor);
	}
	
	@DeleteMapping(value="/{id}")
	public void deleteOne(@PathVariable Long id) {
		coletorService.deletarUm(id);
	}
	
	@GetMapping(value="/empresa/{id}/departamento/{iddep}")
	public List<Coletor> listByEmpresaAndDep(@PathVariable(name="id") Long id, @PathVariable(name="iddep") Long iddep) {
		return coletorService.listarPorEmpresaDep(id, iddep);
	}
	
}
