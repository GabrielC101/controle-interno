package br.com.unoglass.api.ti.coletores.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="COLETORES")
public class Coletor {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long guidColetor;
	private String responsavelColetor;
	private String modeloColetor;
	private String marcaColetor;
	private String serialIdentificador;
	private Long guidEmpresa;
	private Long guidDepartamento;
	
	public Long getGuidColetor() {
		return guidColetor;
	}
	public void setGuidColetor(Long guidColetor) {
		this.guidColetor = guidColetor;
	}
	public String getResponsavelColetor() {
		return responsavelColetor;
	}
	public void setResponsavelColetor(String responsavelColetor) {
		this.responsavelColetor = responsavelColetor;
	}
	public String getModeloColetor() {
		return modeloColetor;
	}
	public void setModeloColetor(String modeloColetor) {
		this.modeloColetor = modeloColetor;
	}
	public String getMarcaColetor() {
		return marcaColetor;
	}
	public void setMarcaColetor(String marcaColetor) {
		this.marcaColetor = marcaColetor;
	}
	public String getSerialIdentificador() {
		return serialIdentificador;
	}
	public void setSerialIdentificador(String serialIdentificador) {
		this.serialIdentificador = serialIdentificador;
	}
	public Long getGuidEmpresa() {
		return guidEmpresa;
	}
	public void setGuidEmpresa(Long guidEmpresa) {
		this.guidEmpresa = guidEmpresa;
	}
	public Long getGuidDepartamento() {
		return guidDepartamento;
	}
	public void setGuidDepartamento(Long guidDepartamento) {
		this.guidDepartamento = guidDepartamento;
	}
	
}
