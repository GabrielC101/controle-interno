package br.com.unoglass.api.ti.coletores.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.unoglass.api.ti.coletores.model.Coletor;

public interface ColetorRepository extends JpaRepository<Coletor, Long> {

	@Query("select c from Coletor c where c.guidEmpresa = ?1 and c.guidDepartamento = ?2")
	List<Coletor> listByEmpresaAndDep(Long id, Long iddep);

}
