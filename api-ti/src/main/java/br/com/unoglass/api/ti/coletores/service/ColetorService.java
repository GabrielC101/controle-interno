package br.com.unoglass.api.ti.coletores.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.coletores.model.Coletor;
import br.com.unoglass.api.ti.coletores.repository.ColetorRepository;
import br.com.unoglass.api.ti.util.BusinessException;
import br.com.unoglass.api.ti.util.BusinessExceptionCode;

@Service
public class ColetorService {

	@Autowired
	ColetorRepository coletorRepository;
	
	public List<Coletor> listarTodos() {
		try {
			return coletorRepository.findAll();
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR011);
		}
	}
	
	public Optional<Coletor> listarUm(Long id) {
		try {
			return coletorRepository.findById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR011);
		}
	}
	
	public Coletor inserirUm(Coletor coletor) {
		try {
			return coletorRepository.save(coletor);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR012);
		}
	}
	
	public void deletarUm(Long id) {
		try {
			coletorRepository.deleteById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR008);
		}
	}

	public List<Coletor> listarPorEmpresaDep(Long id, Long iddep) {
		try {
			return coletorRepository.listByEmpresaAndDep(id, iddep);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR011);
		}
	}
	
	
	
}
