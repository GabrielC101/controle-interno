package br.com.unoglass.api.ti.computadores.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unoglass.api.ti.computadores.model.Computador;
import br.com.unoglass.api.ti.computadores.service.ComputadorService;

@RestController
@RequestMapping(value="/api/ti/computador")
public class ComputadorController {

	@Autowired
	ComputadorService computadorService;
	
	@GetMapping
	public List<Computador> listAll() {
		return computadorService.listarTodos();
	}
	
	@GetMapping(value="/{id}")
	public Optional<Computador> listOne(@PathVariable Long id) {
		return computadorService.listarUm(id);
	}
	
	@PostMapping
	public Computador insertOne(@RequestBody Computador computador) {
		return computadorService.inserirUm(computador);
	}
	
	@DeleteMapping(value="/{id}")
	public void deleteOne(@PathVariable Long id) {
		computadorService.deletarUm(id);
	}
	
	@GetMapping(value="/empresa/{id}/departamento/{idDep}")
	public List<Computador> listComputer(@PathVariable Long id, @PathVariable(name="idDep") Long idDep) {
		return computadorService.listarPorEmpresa(id, idDep);
	}
	
}
