package br.com.unoglass.api.ti.computadores.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="COMPUTADORES")
public class Computador {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long guidComputador;
	private String modeloComputador;
	private Long departamentoComputador;
	private Date dataFabricacao;
	private String serialIdentificador;
	private String sistemaOperacional;
	private String hostMaquina;
	private String ipv4Maquina;
	private Long guidUsuario;
	private Long guidEmpresa;
	private Integer statusAtivo;
	
	public Integer getStatusAtivo() {
		return statusAtivo;
	}
	public void setStatusAtivo(Integer statusAtivo) {
		this.statusAtivo = statusAtivo;
	}
	public Long getGuidEmpresa() {
		return guidEmpresa;
	}
	public void setGuidEmpresa(Long guidEmpresa) {
		this.guidEmpresa = guidEmpresa;
	}
	public Long getGuidComputador() {
		return guidComputador;
	}
	public void setGuidComputador(Long guidComputador) {
		this.guidComputador = guidComputador;
	}
	public String getModeloComputador() {
		return modeloComputador;
	}
	public void setModeloComputador(String modeloComputador) {
		this.modeloComputador = modeloComputador;
	}
	public Long getDepartamentoComputador() {
		return departamentoComputador;
	}
	public void setDepartamentoComputador(Long departamentoComputador) {
		this.departamentoComputador = departamentoComputador;
	}
	public Date getDataFabricacao() {
		return dataFabricacao;
	}
	public void setDataFabricacao(Date dataFabricacao) {
		this.dataFabricacao = dataFabricacao;
	}
	public String getSerialIdentificador() {
		return serialIdentificador;
	}
	public void setSerialIdentificador(String serialIdentificador) {
		this.serialIdentificador = serialIdentificador;
	}
	public String getSistemaOperacional() {
		return sistemaOperacional;
	}
	public void setSistemaOperacional(String sistemaOperacional) {
		this.sistemaOperacional = sistemaOperacional;
	}
	public String getHostMaquina() {
		return hostMaquina;
	}
	public void setHostMaquina(String hostMaquina) {
		this.hostMaquina = hostMaquina;
	}
	public String getIpv4Maquina() {
		return ipv4Maquina;
	}
	public void setIpv4Maquina(String ipv4Maquina) {
		this.ipv4Maquina = ipv4Maquina;
	}
	public Long getGuidUsuario() {
		return guidUsuario;
	}
	public void setGuidUsuario(Long guidUsuario) {
		this.guidUsuario = guidUsuario;
	}
	
}
