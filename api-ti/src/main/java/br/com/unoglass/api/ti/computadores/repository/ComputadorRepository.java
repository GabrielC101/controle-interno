package br.com.unoglass.api.ti.computadores.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.unoglass.api.ti.computadores.model.Computador;

public interface ComputadorRepository extends JpaRepository<Computador, Long> {

	@Query("select c from Computador c where c.guidEmpresa = ?1 and c.departamentoComputador = ?2")
	List<Computador> listByGuidEmpresa(Long id, Long idDep);

}
