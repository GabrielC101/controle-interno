package br.com.unoglass.api.ti.computadores.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.computadores.model.Computador;
import br.com.unoglass.api.ti.computadores.repository.ComputadorRepository;
import br.com.unoglass.api.ti.util.BusinessException;
import br.com.unoglass.api.ti.util.BusinessExceptionCode;

@Service
public class ComputadorService {

	@Autowired
	ComputadorRepository computadorRepository;
	
	public List<Computador> listarTodos() {
		try {
			return computadorRepository.findAll();
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR002);
		}
	}
	
	public Optional<Computador> listarUm(Long id) {
		try {
			return computadorRepository.findById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR002);
		}
	}
	
	public Computador inserirUm(Computador computador) {
		try {
			System.out.println(computador.getStatusAtivo());
			if(computador.getStatusAtivo() == null) {
				computador.setStatusAtivo(1);
				return computadorRepository.save(computador);
			}
			else {
				return computadorRepository.save(computador);
			}
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR003);
		}
	}
	
	public void deletarUm(Long id) {
		try {
			computadorRepository.deleteById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR005);
		}
	}

	public List<Computador> listarPorEmpresa(Long id, Long idDep) {
		try {
			return computadorRepository.listByGuidEmpresa(id, idDep);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR002);
		}
	}
	
}
