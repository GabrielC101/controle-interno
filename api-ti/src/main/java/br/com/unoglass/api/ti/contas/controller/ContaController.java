package br.com.unoglass.api.ti.contas.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unoglass.api.ti.contas.model.Conta;
import br.com.unoglass.api.ti.contas.service.ContaService;

@RestController
@RequestMapping(value="/api/ti/contasapagar")
public class ContaController {

	@Autowired
	ContaService contaService;
	
	@GetMapping
	public List<Conta> listAll() {
		return contaService.listarTodos();
	}
	
	@GetMapping(value="/{id}")
	public Optional<Conta> listOne(@PathVariable Long id) {
		return contaService.listarUm(id);
	}
	
	@PostMapping
	public Conta insertOne(@RequestBody Conta conta) {
		return contaService.inserirUm(conta);
	}
	
	@DeleteMapping(value="/{id}")
	public void deleteOne(@PathVariable Long id) {
		contaService.deletarUm(id);
	}
	
	@GetMapping(value="/empresa/{idEmpresa}")
	public List<Conta> listByEmpresaAndDep(@PathVariable(name="idEmpresa") Long idEmpresa) {
		return contaService.listarPorEmpresaEDepartamento(idEmpresa);
	}
	
}
