package br.com.unoglass.api.ti.contas.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="CONTASAPAGAR")
public class Conta {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long guidContas;
	private String nomeConta;
	private String siteConta;
	private Integer identificadorMensal;
	private String loginConta;
	private String passwordConta;
	private Long guidTipoConta;
	private Long guidEmpresa;
	private LocalDate dataVencimento;
	
	public LocalDate getDataVencimento() {
		return dataVencimento;
	}
	public void setDataVencimento(LocalDate dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	public String getNomeConta() {
		return nomeConta;
	}
	public void setNomeConta(String nomeConta) {
		this.nomeConta = nomeConta;
	}
	public Long getGuidEmpresa() {
		return guidEmpresa;
	}
	public void setGuidEmpresa(Long guidEmpresa) {
		this.guidEmpresa = guidEmpresa;
	}
	public Long getGuidContas() {
		return guidContas;
	}
	public void setGuidContas(Long guidContas) {
		this.guidContas = guidContas;
	}
	public String getSiteConta() {
		return siteConta;
	}
	public void setSiteConta(String siteConta) {
		this.siteConta = siteConta;
	}
	public Integer getIdentificadorMensal() {
		return identificadorMensal;
	}
	public void setIdentificadorMensal(Integer identificadorMensal) {
		this.identificadorMensal = identificadorMensal;
	}
	public String getLoginConta() {
		return loginConta;
	}
	public void setLoginConta(String loginConta) {
		this.loginConta = loginConta;
	}
	public String getPasswordConta() {
		return passwordConta;
	}
	public void setPasswordConta(String passwordConta) {
		this.passwordConta = passwordConta;
	}
	public Long getGuidTipoConta() {
		return guidTipoConta;
	}
	public void setGuidTipoConta(Long guidTipoConta) {
		this.guidTipoConta = guidTipoConta;
	}
	
}
