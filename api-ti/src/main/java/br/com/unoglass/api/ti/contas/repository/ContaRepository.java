package br.com.unoglass.api.ti.contas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.unoglass.api.ti.contas.model.Conta;

public interface ContaRepository extends JpaRepository<Conta, Long> {
	
	@Query("select c from Conta c where c.guidEmpresa = ?1")
	List<Conta> findByEmpresaAndDepartamento(Long idEmpresa);

}
