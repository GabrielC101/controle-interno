package br.com.unoglass.api.ti.contas.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.contas.model.Conta;
import br.com.unoglass.api.ti.contas.repository.ContaRepository;
import br.com.unoglass.api.ti.util.BusinessException;
import br.com.unoglass.api.ti.util.BusinessExceptionCode;

@Service
public class ContaService {

	@Autowired
	ContaRepository contaRepository;
	
	public List<Conta> listarTodos() {
		try {
			return contaRepository.findAll();
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR013);
		}
	}
	
	public Optional<Conta> listarUm(Long id) {
		try {
			return contaRepository.findById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR013);
		}
	}
	
	public Conta inserirUm(Conta conta) {
		try {
			return contaRepository.save(conta);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR014);
		}
	}
	
	public void deletarUm(Long id) {
		try {
			contaRepository.deleteById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR008);
		}
	}

	public List<Conta> listarPorEmpresaEDepartamento(Long idEmpresa) {
		try {
			return contaRepository.findByEmpresaAndDepartamento(idEmpresa);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR013);
		}
	}
	
}
