package br.com.unoglass.api.ti.departamento.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unoglass.api.ti.departamento.model.Departamento;
import br.com.unoglass.api.ti.departamento.service.DepartamentoService;

@RestController
@RequestMapping(value="/api/ti/departamento")
public class DepartamentoController {
	
	@Autowired
	DepartamentoService departamentoService;
	
	@GetMapping
	public List<Departamento> listAll() {
		return departamentoService.listarTodos();
	}
	
	@GetMapping(value="/{id}")
	public Optional<Departamento> listOne(@PathVariable Long id) {
		return departamentoService.listarUm(id);
	}
	
	@PostMapping
	public Departamento insertOne(@RequestBody Departamento departamento) {
		return departamentoService.inserirUm(departamento);
	}
	
	@DeleteMapping(value="/{id}")
	public void deleteOne(@PathVariable Long id) {
		departamentoService.deletarUm(id);
	}
	
}
