package br.com.unoglass.api.ti.departamento.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DEPARTAMENTO")
public class Departamento {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long guidDepartamento;
	private String nomeDepartamento;
	private String siglaDepartamento;
	
	public Long getGuidDepartamento() {
		return guidDepartamento;
	}
	public void setGuidDepartamento(Long guidDepartamento) {
		this.guidDepartamento = guidDepartamento;
	}
	public String getNomeDepartamento() {
		return nomeDepartamento;
	}
	public void setNomeDepartamento(String nomeDepartamento) {
		this.nomeDepartamento = nomeDepartamento;
	}
	public String getSiglaDepartamento() {
		return siglaDepartamento;
	}
	public void setSiglaDepartamento(String siglaDepartamento) {
		this.siglaDepartamento = siglaDepartamento;
	}
	
}
