package br.com.unoglass.api.ti.departamento.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.unoglass.api.ti.departamento.model.Departamento;

public interface DepartamentoRepository extends JpaRepository<Departamento, Long>{

}
