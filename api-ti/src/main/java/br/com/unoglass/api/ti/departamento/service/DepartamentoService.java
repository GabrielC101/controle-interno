package br.com.unoglass.api.ti.departamento.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.departamento.model.Departamento;
import br.com.unoglass.api.ti.departamento.repository.DepartamentoRepository;
import br.com.unoglass.api.ti.util.BusinessException;
import br.com.unoglass.api.ti.util.BusinessExceptionCode;

@Service
public class DepartamentoService {

	@Autowired
	DepartamentoRepository departamentoRepository;
	
	public List<Departamento> listarTodos() {
		try {
			return departamentoRepository.findAll();
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR006);
		}
	}
	
	public Optional<Departamento> listarUm(Long id) {
		try {
			return departamentoRepository.findById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR006);
		}
	}
	
	public Departamento inserirUm(Departamento departamento) {
		try {
			return departamentoRepository.save(departamento);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR007);
		}
	}
	
	public void deletarUm(Long id) {
		try {
			departamentoRepository.deleteById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR008);
		}
	}
	
}
