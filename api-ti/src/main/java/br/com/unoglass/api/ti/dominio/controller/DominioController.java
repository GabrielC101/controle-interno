package br.com.unoglass.api.ti.dominio.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unoglass.api.ti.dominio.model.Dominio;
import br.com.unoglass.api.ti.dominio.service.DominioService;

@RestController
@RequestMapping(value="/api/ti/dominio")
public class DominioController {

	@Autowired
	DominioService dominioService;
	
	@GetMapping
	public List<Dominio> listAll() {
		return dominioService.listarTodos();
	}
	
	@GetMapping(value="/{id}")
	public Optional<Dominio> listOne(@PathVariable Long id) {
		return dominioService.listarUm(id);
	}
	
	@PostMapping
	public Dominio insertOne(@RequestBody Dominio dominio) {
		return dominioService.inserirUm(dominio);
	}
	
	@DeleteMapping(value="/{id}")
	public void deleteOne(@PathVariable Long id) {
		dominioService.deletarUm(id);
	}
	
	@GetMapping(value="/empresa/{id}")
	public List<Dominio> listByEmpresa(@PathVariable Long id) {
		return dominioService.findByEmpresa(id);
	}
	
}
