package br.com.unoglass.api.ti.dominio.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DOMINIO")
public class Dominio {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long guidDominio;
	private String urlDominio;
	private String urlSSLDominio;
	private String linkFTP;
	private String usuarioFTP;
	private String passwordFTP;
	private Long guidEmpresa;
	
	public Long getGuidEmpresa() {
		return guidEmpresa;
	}
	public void setGuidEmpresa(Long guidEmpresa) {
		this.guidEmpresa = guidEmpresa;
	}
	public Long getGuidDominio() {
		return guidDominio;
	}
	public void setGuidDominio(Long guidDominio) {
		this.guidDominio = guidDominio;
	}
	public String getUrlDominio() {
		return urlDominio;
	}
	public void setUrlDominio(String urlDominio) {
		this.urlDominio = urlDominio;
	}
	public String getUrlSSLDominio() {
		return urlSSLDominio;
	}
	public void setUrlSSLDominio(String urlSSLDominio) {
		this.urlSSLDominio = urlSSLDominio;
	}
	public String getLinkFTP() {
		return linkFTP;
	}
	public void setLinkFTP(String linkFTP) {
		this.linkFTP = linkFTP;
	}
	public String getUsuarioFTP() {
		return usuarioFTP;
	}
	public void setUsuarioFTP(String usuarioFTP) {
		this.usuarioFTP = usuarioFTP;
	}
	public String getPasswordFTP() {
		return passwordFTP;
	}
	public void setPasswordFTP(String passwordFTP) {
		this.passwordFTP = passwordFTP;
	}
	
}
