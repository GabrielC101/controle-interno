package br.com.unoglass.api.ti.dominio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.unoglass.api.ti.dominio.model.Dominio;

public interface DominioRepository extends JpaRepository<Dominio, Long>{

	@Query("select d from Dominio d where d.guidEmpresa = ?1")
	List<Dominio> findByEmpresa(Long id);

}
