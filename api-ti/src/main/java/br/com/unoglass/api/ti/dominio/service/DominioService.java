package br.com.unoglass.api.ti.dominio.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.dominio.model.Dominio;
import br.com.unoglass.api.ti.dominio.repository.DominioRepository;
import br.com.unoglass.api.ti.util.BusinessException;
import br.com.unoglass.api.ti.util.BusinessExceptionCode;

@Service
public class DominioService {

	@Autowired
	DominioRepository dominioRepository;
	
	public List<Dominio> listarTodos() {
		try {
			return dominioRepository.findAll();
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR020);
		}
	}
	
	public Optional<Dominio> listarUm(Long id) {
		try {
			return dominioRepository.findById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR020);
		}
	}
	
	public Dominio inserirUm(Dominio dominio) {
		try {
			return dominioRepository.save(dominio);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR021);
		}
	}
	
	public void deletarUm(Long id) {
		try {
			dominioRepository.deleteById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR008);
		}
	}

	public List<Dominio> findByEmpresa(Long id) {
		try {
			return dominioRepository.findByEmpresa(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR020);
		}
	}
	
}
