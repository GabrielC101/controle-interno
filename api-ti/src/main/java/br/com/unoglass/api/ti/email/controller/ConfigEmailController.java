package br.com.unoglass.api.ti.email.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unoglass.api.ti.email.model.ConfigEmail;
import br.com.unoglass.api.ti.email.model.Mensagem;
import br.com.unoglass.api.ti.email.service.ConfigEmailService;

@RestController
@RequestMapping(value="/api/ti/config/email")
public class ConfigEmailController {

	@Autowired
	ConfigEmailService configEmailService;
	
	@GetMapping
	public Optional<ConfigEmail> findById() {
		Long id = (long) 1;
		return configEmailService.configurarEmail(id);
	}
	
	@PostMapping
	public ConfigEmail insertConfigOrUpdate(@RequestBody ConfigEmail configEmail) {
		return configEmailService.insertConfig(configEmail);
	}
	
	@PostMapping(value="/mensagem")
	public void postEmail(@RequestBody Mensagem mensagem) {
		System.out.println("CHEGOUUUUUU");
		configEmailService.enviarMensagem(mensagem);
	}
	
}
