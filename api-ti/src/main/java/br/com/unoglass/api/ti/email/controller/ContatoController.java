package br.com.unoglass.api.ti.email.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unoglass.api.ti.email.model.Contato;
import br.com.unoglass.api.ti.email.service.ContatoService;

@RestController
@RequestMapping(value="/api/ti/contato")
public class ContatoController {

	@Autowired
	ContatoService contatoService;
	
	@GetMapping
	public List<Contato> listAll() {
		return contatoService.listarTodos();
	}
	
	@GetMapping(value="/{id}")
	public Optional<Contato> listOne(@PathVariable Long id) {
		return contatoService.listarUm(id);
	}
	
	@PostMapping
	public Contato insertOne(@RequestBody Contato contato) {
		return contatoService.inserirUm(contato);
	}
	
	@DeleteMapping(value="/{id}")
	public void deleteOne(@PathVariable Long id) {
		contatoService.deletarUm(id);
	}
	
	@GetMapping(value="/grupo/{id}")
	public List<Contato> listContatosOfGrupo(@PathVariable Long id) {
		return contatoService.findByGrupo(id);
	}
	
}
