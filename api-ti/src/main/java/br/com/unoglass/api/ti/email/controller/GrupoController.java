package br.com.unoglass.api.ti.email.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unoglass.api.ti.email.model.Grupo;
import br.com.unoglass.api.ti.email.service.GrupoService;

@RestController
@RequestMapping(value="/api/ti/grupo")
public class GrupoController {

	@Autowired
	GrupoService grupoService;
	
	@GetMapping
	public List<Grupo> listGrupos() {
		return grupoService.listGrupos();
	}
	
	@PostMapping
	public Grupo insertGrupo(@RequestBody Grupo grupo) {
		return grupoService.insertGrupo(grupo);
	}
	
	@DeleteMapping(value="/{id}")
	public void deleteGrupo(@PathVariable Long id) {
		grupoService.deletarGrupo(id);
	}
	
}
