package br.com.unoglass.api.ti.email.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="EMAILCONFIG")
public class ConfigEmail {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long guidEmailConfig;
	private String hostEmail;
	private String portEmail;
	private String smtpPort;
	private String emailAddress;
	private String passwordEmail;
	
	public Long getGuidEmailConfig() {
		return guidEmailConfig;
	}
	public void setGuidEmailConfig(Long guidEmailConfig) {
		this.guidEmailConfig = guidEmailConfig;
	}
	public String getHostEmail() {
		return hostEmail;
	}
	public void setHostEmail(String hostEmail) {
		this.hostEmail = hostEmail;
	}
	public String getPortEmail() {
		return portEmail;
	}
	public void setPortEmail(String portEmail) {
		this.portEmail = portEmail;
	}
	public String getSmtpPort() {
		return smtpPort;
	}
	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPasswordEmail() {
		return passwordEmail;
	}
	public void setPasswordEmail(String passwordEmail) {
		this.passwordEmail = passwordEmail;
	}
	
}
