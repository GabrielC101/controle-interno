package br.com.unoglass.api.ti.email.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="EMAILCONTATO")
public class Contato {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long guidContato;
	private Long guidGrupo;
	private String nomeContato;
	private String enderecoEmail;
	
	public Long getGuidContato() {
		return guidContato;
	}
	public void setGuidContato(Long guidContato) {
		this.guidContato = guidContato;
	}
	public Long getGuidGrupo() {
		return guidGrupo;
	}
	public void setGuidGrupo(Long guidGrupo) {
		this.guidGrupo = guidGrupo;
	}
	public String getNomeContato() {
		return nomeContato;
	}
	public void setNomeContato(String nomeContato) {
		this.nomeContato = nomeContato;
	}
	public String getEnderecoEmail() {
		return enderecoEmail;
	}
	public void setEnderecoEmail(String enderecoEmail) {
		this.enderecoEmail = enderecoEmail;
	}
	
}
