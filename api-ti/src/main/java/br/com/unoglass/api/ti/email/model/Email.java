package br.com.unoglass.api.ti.email.model;

import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.unoglass.api.ti.email.service.ConfigEmailService;

public class Email {
	
	@Autowired
	ConfigEmailService configEmailService;
	
	private String mailSMTPServer;
	private String mailSMTPServerPort;
	private String mailAddress;
	private String passwordEmail;
	
	public Email(String mailSMTPServer, String mailSMTPServerPort, String email, String pass) { 
		this.mailSMTPServer = mailSMTPServer;
		this.mailSMTPServerPort = mailSMTPServerPort;
		this.mailAddress = email;
		this.passwordEmail = pass;
	}
	
	public void sendMail(List<Contato> listaContato, String subject, String message) {
		
		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtp"); 
		props.put("mail.smtp.starttls.enable","true"); 
		props.put("mail.smtp.host", mailSMTPServer); 
		props.put("mail.smtp.auth", "true"); 
		props.put("mail.smtp.user", mailAddress); 
		props.put("mail.debug", "true");
		props.put("mail.smtp.port", mailSMTPServerPort); 
		props.put("mail.smtp.socketFactory.port", mailSMTPServerPort); 
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.fallback", "false");
		@SuppressWarnings("unused")
		SimpleAuth auth = null;
		auth = new SimpleAuth (mailAddress, passwordEmail);
		Session session = Session.getDefaultInstance(props,      new javax.mail.Authenticator() {
	           protected PasswordAuthentication getPasswordAuthentication() 
	           {
	                 return new PasswordAuthentication(mailAddress, passwordEmail);
	           }
	      });
		session.setDebug(true); 
		Message msg = new MimeMessage(session);
		String[] mailAddressTo = new String[listaContato.size()];
		try {
			for(int x = 0; x < listaContato.size(); x++) {
				mailAddressTo[x] = listaContato.get(x).getEnderecoEmail();
			}
			InternetAddress[] mailAddress_TO = new InternetAddress[mailAddressTo.length];
			for (int i = 0; i < mailAddressTo.length; i++) {
			    mailAddress_TO[i] = new InternetAddress(mailAddressTo[i]);
			}
			msg.setRecipients(Message.RecipientType.TO, mailAddress_TO);
			msg.setFrom(new InternetAddress(mailAddress));
			msg.setSubject(subject);
			msg.setContent(message,"text/plain");
		} catch (Exception e) {
			System.out.println(">> Erro: Completar Mensagem");
			e.printStackTrace();
		}
		Transport tr;
		try {
			tr = session.getTransport("smtp"); 
			tr.connect(mailSMTPServer, mailAddress, passwordEmail);
			msg.saveChanges(); // don't forget this
			tr.sendMessage(msg, msg.getAllRecipients());
			tr.close();
		} catch (Exception e) {
			System.out.println(">> Erro: Envio Mensagem");
			e.printStackTrace();
		}
	}
}

class SimpleAuth extends Authenticator {
	public String username = null;
	public String password = null;


	public SimpleAuth(String user, String pwd) {
		username = user;
		password = pwd;
	}

	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication (username,password);
	}
} 
