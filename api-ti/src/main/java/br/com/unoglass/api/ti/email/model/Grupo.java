package br.com.unoglass.api.ti.email.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="EMAILGRUPO")
public class Grupo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long guidGrupo;
	private String descricaoGrupo;
	
	public Long getGuidGrupo() {
		return guidGrupo;
	}
	public void setGuidGrupo(Long guidGrupo) {
		this.guidGrupo = guidGrupo;
	}
	public String getDescricaoGrupo() {
		return descricaoGrupo;
	}
	public void setDescricaoGrupo(String descricaoGrupo) {
		this.descricaoGrupo = descricaoGrupo;
	}
	
}
