package br.com.unoglass.api.ti.email.model;

public class Mensagem {
	
	private String mensagemWeb;
	private Long guidGrupo;
	
	public String getMensagemWeb() {
		return mensagemWeb;
	}
	public void setMensagemWeb(String mensagemWeb) {
		this.mensagemWeb = mensagemWeb;
	}
	public Long getGuidGrupo() {
		return guidGrupo;
	}
	public void setGuidGrupo(Long guidGrupo) {
		this.guidGrupo = guidGrupo;
	}
	
}
