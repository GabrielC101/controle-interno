package br.com.unoglass.api.ti.email.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.unoglass.api.ti.email.model.ConfigEmail;

public interface ConfigEmailRepository extends JpaRepository<ConfigEmail, Long>{

	@Query("select c from ConfigEmail c where c.guidEmailConfig = 1")
	ConfigEmail acharConfig();

}
