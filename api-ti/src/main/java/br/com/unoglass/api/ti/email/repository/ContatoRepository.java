package br.com.unoglass.api.ti.email.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import br.com.unoglass.api.ti.email.model.Contato;

public interface ContatoRepository extends JpaRepository <Contato, Long>{

	@Transactional
	@Modifying
	@Query("delete from Contato c where c.guidGrupo = ?1")
	void deleteByIdGrupo(Long id);

	@Query("select c from Contato c where c.guidGrupo = ?1")
	List<Contato> findByGrupoId(Long id);

}
