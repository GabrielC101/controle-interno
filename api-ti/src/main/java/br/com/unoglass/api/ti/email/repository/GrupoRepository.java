package br.com.unoglass.api.ti.email.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.unoglass.api.ti.email.model.Grupo;

public interface GrupoRepository extends JpaRepository<Grupo, Long>{

}
