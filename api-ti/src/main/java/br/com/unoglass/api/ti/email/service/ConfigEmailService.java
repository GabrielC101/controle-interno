package br.com.unoglass.api.ti.email.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.email.model.ConfigEmail;
import br.com.unoglass.api.ti.email.model.Contato;
import br.com.unoglass.api.ti.email.model.Email;
import br.com.unoglass.api.ti.email.model.Mensagem;
import br.com.unoglass.api.ti.email.repository.ConfigEmailRepository;
import br.com.unoglass.api.ti.util.BusinessException;
import br.com.unoglass.api.ti.util.BusinessExceptionCode;

@Service
public class ConfigEmailService {

	@Autowired
	ConfigEmailRepository configEmailRepository;
	
	@Autowired
	ContatoService contatoService;
	
	public Optional<ConfigEmail> configurarEmail(Long id) {
		try {
			return configEmailRepository.findById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR022);
		}
	}
	
	public ConfigEmail insertConfig(ConfigEmail configEmail) {
		try {
			return configEmailRepository.save(configEmail);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR023);
		}
	}
	
	public ConfigEmail acharConfig() {
		return configEmailRepository.acharConfig();
	}

	public void enviarMensagem(Mensagem mensagem) {
		System.out.println("CHEGOU AQUI TMBBBBBBBBBBBB");
		System.out.println(mensagem.getMensagemWeb());
		System.out.println(mensagem.getGuidGrupo());
		List<Contato> listaContato = contatoService.findByGrupo(mensagem.getGuidGrupo());
		System.out.println(listaContato.size());
		ConfigEmail configEmail = acharConfig();
		Email email = new Email(configEmail.getHostEmail(), configEmail.getSmtpPort(), configEmail.getEmailAddress(), configEmail.getPasswordEmail());
		email.sendMail(listaContato, "Controle Interno - EMAIL INTERNO", mensagem.getMensagemWeb());
		
	}
	
}
