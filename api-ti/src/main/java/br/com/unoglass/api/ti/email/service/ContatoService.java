package br.com.unoglass.api.ti.email.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.email.model.Contato;
import br.com.unoglass.api.ti.email.repository.ContatoRepository;
import br.com.unoglass.api.ti.util.BusinessException;
import br.com.unoglass.api.ti.util.BusinessExceptionCode;

@Service
public class ContatoService {

	@Autowired
	ContatoRepository contatoRepository;
	
	public List<Contato> listarTodos() {
		try {
			return contatoRepository.findAll();
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR026);
		}
	}
	
	public Optional<Contato> listarUm(Long id) {
		try {
			return contatoRepository.findById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR026);
		}
	}
	
	public Contato inserirUm(Contato contato) {
		try {
			return contatoRepository.save(contato);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR027);
		}
	}
	
	public void deletarUm(Long id) {
		try {
			contatoRepository.deleteById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR008);
		}
	}
	
	@Transactional
	public void deletarCascata(Long id) {
		try {
			contatoRepository.deleteByIdGrupo(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR008);
		}
	}

	public List<Contato> findByGrupo(Long id) {
		try {
			return contatoRepository.findByGrupoId(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR026);
		}
	}
	
}
