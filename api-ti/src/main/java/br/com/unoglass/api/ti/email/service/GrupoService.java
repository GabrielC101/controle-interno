package br.com.unoglass.api.ti.email.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.email.model.Grupo;
import br.com.unoglass.api.ti.email.repository.GrupoRepository;
import br.com.unoglass.api.ti.util.BusinessException;
import br.com.unoglass.api.ti.util.BusinessExceptionCode;

@Service
public class GrupoService {

	@Autowired
	GrupoRepository grupoRepository;
	
	@Autowired
	ContatoService contatoService;
	
	public List<Grupo> listGrupos() {
		try {
			return grupoRepository.findAll();
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR024);
		}
	}
	
	public Grupo insertGrupo(Grupo grupo) {
		try {
			return grupoRepository.save(grupo);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR025);
		}
	}
	
	public void deletarGrupo(Long id) {
		try {
			contatoService.deletarCascata(id);
			grupoRepository.deleteById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR008);
		}
	}
	
}
