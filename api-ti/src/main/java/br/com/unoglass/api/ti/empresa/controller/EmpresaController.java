package br.com.unoglass.api.ti.empresa.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unoglass.api.ti.empresa.model.Empresa;
import br.com.unoglass.api.ti.empresa.service.EmpresaService;

@RestController
@RequestMapping(value="/api/ti/empresa")
public class EmpresaController {

	@Autowired
	EmpresaService empresaService;
	
	@GetMapping
	public List<Empresa> listAll() {
		return empresaService.listarTodos();
	}
	
	@GetMapping(value="/{id}")
	public Optional<Empresa> listOne(@PathVariable Long id) {
		return empresaService.listarUm(id);
	}
	
	@PostMapping
	public Empresa insertOne(@RequestBody Empresa empresa) {
		return empresaService.inserirUm(empresa);
	}
	
	@DeleteMapping(value="/{id}")
	public void deleteOne(@PathVariable Long id) {
		empresaService.deletarUm(id);
	}
	
}
