package br.com.unoglass.api.ti.empresa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="EMPRESA")
public class Empresa {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long guidEmpresa;
	private String nomeEmpresa;
	private String razaoSocial;
	private String cnpjEmpresa;
	private Integer identificadorMatriz;
	
	public Long getGuidEmpresa() {
		return guidEmpresa;
	}
	public void setGuidEmpresa(Long guidEmpresa) {
		this.guidEmpresa = guidEmpresa;
	}
	public String getNomeEmpresa() {
		return nomeEmpresa;
	}
	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getCnpjEmpresa() {
		return cnpjEmpresa;
	}
	public void setCnpjEmpresa(String cnpjEmpresa) {
		this.cnpjEmpresa = cnpjEmpresa;
	}
	public Integer getIdentificadorMatriz() {
		return identificadorMatriz;
	}
	public void setIdentificadorMatriz(Integer identificadorMatriz) {
		this.identificadorMatriz = identificadorMatriz;
	}
	
}
