package br.com.unoglass.api.ti.empresa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.unoglass.api.ti.empresa.model.Empresa;

public interface EmpresaRepository extends JpaRepository<Empresa, Long> {

}
