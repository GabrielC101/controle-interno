package br.com.unoglass.api.ti.empresa.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.empresa.model.Empresa;
import br.com.unoglass.api.ti.empresa.repository.EmpresaRepository;
import br.com.unoglass.api.ti.util.BusinessException;
import br.com.unoglass.api.ti.util.BusinessExceptionCode;

@Service
public class EmpresaService {

	@Autowired
	EmpresaRepository empresaRepository;
	
	public List<Empresa> listarTodos() {
		try {
			return empresaRepository.findAll();
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR009);
		}
	}
	
	public Optional<Empresa> listarUm(Long id) {
		try {
			return empresaRepository.findById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR009);
		}
	}
	
	public Empresa inserirUm(Empresa empresa) {
		try {
			return empresaRepository.save(empresa);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR010);
		}
	}
	
	public void deletarUm(Long id) {
		try {
			empresaRepository.deleteById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR008);
		}
	}
	
}
