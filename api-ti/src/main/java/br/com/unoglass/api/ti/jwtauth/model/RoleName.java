package br.com.unoglass.api.ti.jwtauth.model;

public enum  RoleName {
    ROLE_USER,
    ROLE_PM,
    ROLE_ADMIN
}