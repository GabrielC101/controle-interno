package br.com.unoglass.api.ti.jwtauth.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.unoglass.api.ti.jwtauth.model.Role;
import br.com.unoglass.api.ti.jwtauth.model.RoleName;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
    
    @Query("select r from Role r where r.id = ?1")
    Role findByIdRole(Long id);
    
}