package br.com.unoglass.api.ti.manutencao.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unoglass.api.ti.manutencao.model.Manutencao;
import br.com.unoglass.api.ti.manutencao.service.ManutencaoService;

@RestController
@RequestMapping(value="/api/ti/manutencao")
public class ManutencaoController {

	@Autowired
	ManutencaoService manutencaoService;
	
	@PreAuthorize("hasRole('ROLE_USER')")
	@GetMapping
	public List<Manutencao> listAll() {
		return manutencaoService.listarTodos();
	}
	
	@GetMapping(value="/{id}")
	public Optional<Manutencao> listOne(@PathVariable Long id) {
		return manutencaoService.listarUm(id);
	}
	
	@PostMapping
	public Manutencao insertOne(@RequestBody Manutencao manutencao) {
		manutencao.setDataManutencao(LocalDateTime.now());
		return manutencaoService.inserirUm(manutencao);
	}
	
	@DeleteMapping(value="/{id}")
	public void deleteOne(@PathVariable Long id) {
		manutencaoService.deletarUm(id);
	}
	
	@GetMapping(value="/periferico/{id}")
	public List<Manutencao> listByManutencaoObjeto(@PathVariable Long id) {
		return manutencaoService.listarPorPeriferico(id);
	}
	
	
	
}
