package br.com.unoglass.api.ti.manutencao.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MANUTENCAOCOLETOR")
public class ManutencaoColetor {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long guidManutencaoColetor;
	private String nomeManutencao;
	private Double precoManutencao;
	private Long guidNotaFiscal;
	private String detalhesManutencao;
	private Long guidEquipamento;
	private Long guidEmpresa;
	private String tokenIdentificacao;
	private LocalDateTime dataManutencao;
	
	public LocalDateTime getDataManutencao() {
		return dataManutencao;
	}
	public void setDataManutencao(LocalDateTime localDateTime) {
		this.dataManutencao = localDateTime;
	}
	public Long getGuidManutencaoColetor() {
		return guidManutencaoColetor;
	}
	public void setGuidManutencaoColetor(Long guidManutencaoColetor) {
		this.guidManutencaoColetor = guidManutencaoColetor;
	}
	public String getNomeManutencao() {
		return nomeManutencao;
	}
	public void setNomeManutencao(String nomeManutencao) {
		this.nomeManutencao = nomeManutencao;
	}
	public Double getPrecoManutencao() {
		return precoManutencao;
	}
	public void setPrecoManutencao(Double precoManutencao) {
		this.precoManutencao = precoManutencao;
	}
	public Long getGuidNotaFiscal() {
		return guidNotaFiscal;
	}
	public void setGuidNotaFiscal(Long guidNotaFiscal) {
		this.guidNotaFiscal = guidNotaFiscal;
	}
	public String getDetalhesManutencao() {
		return detalhesManutencao;
	}
	public void setDetalhesManutencao(String detalhesManutencao) {
		this.detalhesManutencao = detalhesManutencao;
	}
	public Long getGuidEquipamento() {
		return guidEquipamento;
	}
	public void setGuidEquipamento(Long guidEquipamento) {
		this.guidEquipamento = guidEquipamento;
	}
	public Long getGuidEmpresa() {
		return guidEmpresa;
	}
	public void setGuidEmpresa(Long guidEmpresa) {
		this.guidEmpresa = guidEmpresa;
	}
	public String getTokenIdentificacao() {
		return tokenIdentificacao;
	}
	public void setTokenIdentificacao(String tokenIdentificacao) {
		this.tokenIdentificacao = tokenIdentificacao;
	}
	
}
