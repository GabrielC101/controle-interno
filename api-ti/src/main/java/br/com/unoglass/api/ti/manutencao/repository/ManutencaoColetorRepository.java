package br.com.unoglass.api.ti.manutencao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.unoglass.api.ti.manutencao.model.ManutencaoColetor;

public interface ManutencaoColetorRepository extends JpaRepository<ManutencaoColetor, Long>{
	
	@Query("select m from ManutencaoColetor m where m.guidEquipamento = ?1")
	List<ManutencaoColetor> listByIdPeriferico(Long id);
	
}
