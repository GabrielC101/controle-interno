package br.com.unoglass.api.ti.manutencao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.unoglass.api.ti.manutencao.model.Manutencao;

public interface ManutencaoRepository extends JpaRepository<Manutencao, Long> {

	@Query("select m from Manutencao m where m.guidEquipamento = ?1")
	List<Manutencao> listByIdPeriferico(Long id);

}
