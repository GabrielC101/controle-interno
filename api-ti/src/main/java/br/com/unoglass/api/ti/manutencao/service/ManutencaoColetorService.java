package br.com.unoglass.api.ti.manutencao.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.manutencao.model.ManutencaoColetor;
import br.com.unoglass.api.ti.manutencao.repository.ManutencaoColetorRepository;
import br.com.unoglass.api.ti.util.BusinessException;
import br.com.unoglass.api.ti.util.BusinessExceptionCode;

@Service
public class ManutencaoColetorService {
	
	@Autowired
	ManutencaoColetorRepository manutencaoRepository;
	
	public List<ManutencaoColetor> listarTodos() {
		try {
			return manutencaoRepository.findAll();
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR017);
		}
	}
	
	public Optional<ManutencaoColetor> listarUm(Long id) {
		try {
			return manutencaoRepository.findById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR017);
		}
	}
	
	public ManutencaoColetor inserirUm(ManutencaoColetor manutencao) {
		try {
			return manutencaoRepository.save(manutencao);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR018);
		}
	}
	
	public void deletarUm(Long id) {
		try {
			manutencaoRepository.deleteById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR008);
		}
	}

	public List<ManutencaoColetor> listarPorPeriferico(Long id) {
		try {
			return manutencaoRepository.listByIdPeriferico(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR017);
		}
	}
	
}
