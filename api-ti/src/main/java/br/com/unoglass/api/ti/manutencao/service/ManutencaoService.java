package br.com.unoglass.api.ti.manutencao.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.manutencao.model.Manutencao;
import br.com.unoglass.api.ti.manutencao.repository.ManutencaoRepository;
import br.com.unoglass.api.ti.util.BusinessException;
import br.com.unoglass.api.ti.util.BusinessExceptionCode;

@Service
public class ManutencaoService {

	@Autowired
	ManutencaoRepository manutencaoRepository;
	
	public List<Manutencao> listarTodos() {
		try {
			return manutencaoRepository.findAll();
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR017);
		}
	}
	
	public Optional<Manutencao> listarUm(Long id) {
		try {
			return manutencaoRepository.findById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR017);
		}
	}
	
	public Manutencao inserirUm(Manutencao manutencao) {
		try {
			return manutencaoRepository.save(manutencao);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR018);
		}
	}
	
	public void deletarUm(Long id) {
		try {
			manutencaoRepository.deleteById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR008);
		}
	}

	public List<Manutencao> listarPorPeriferico(Long id) {
		try {
			return manutencaoRepository.listByIdPeriferico(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR017);
		}
	}
	
}
