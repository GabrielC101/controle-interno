package br.com.unoglass.api.ti.notasfiscais.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.unoglass.api.ti.notasfiscais.model.NotasFiscais;
import br.com.unoglass.api.ti.notasfiscais.service.NotasFiscaisService;

@RestController
@RequestMapping(value="/api/ti/notafiscal")
public class NotasFiscaisController {
	
	@Autowired
	NotasFiscaisService notasFiscaisService;
	
	@CrossOrigin
	@PostMapping(value="/{id}/token/{idToken}", consumes = "multipart/form-data")
	public NotasFiscais salvar(@PathVariable(name="id") Long id, @PathVariable(name="idToken") String idToken, 
			@RequestParam("arquivo") MultipartFile file) {
		NotasFiscais arquivo = new NotasFiscais();
		try {			
			arquivo.setContentType(file.getContentType());
			arquivo.setNomeArquivo(file.getOriginalFilename());
			arquivo.setConteudoArquivo(file.getBytes());
			arquivo.setGuidEquipamento(id);
			arquivo.setTokenIdentificacao(idToken);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return notasFiscaisService.save(arquivo);
	}
	
	@GetMapping("/push/{guidArquivo}")
	public NotasFiscais findOneById(@PathVariable("guidArquivo") Long guidArquivo) {
		return notasFiscaisService.findOneById(guidArquivo);
	}
	
	@GetMapping("/download/{guidArquivo}")
	public ResponseEntity<Resource> download(@PathVariable("guidArquivo") Long guidArquivo) {
		NotasFiscais arquivo = notasFiscaisService.findOneById(guidArquivo);
		Resource resource = new ByteArrayResource(arquivo.getConteudoArquivo());
		String contentType = arquivo.getContentType();
		if (contentType == null) {
			contentType = "application/octet-stream";
		}
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + 
						arquivo.getNomeArquivo() + "\"")
				.body(resource);
	}
	
	@GetMapping(value="/{id}")
	public NotasFiscais findByIdEquipamento(@PathVariable Long id) {
		return notasFiscaisService.findByIdEquipamento(id);
	}
	
	@DeleteMapping(value="{id}")
	public void deleteById(@PathVariable Long id) {
		notasFiscaisService.deleteNota(id);
	}
	
	@GetMapping(value="/token/{token}")
	public NotasFiscais findByTokenIdf(@PathVariable String token) {
		return notasFiscaisService.findByTokenIdf(token);
	}
	
	@GetMapping(value="/empresa/{id}")
	public List<NotasFiscais> findByEmpresa(@PathVariable Long id) {
		return notasFiscaisService.findByEmpresa(id);
	}
	
	@CrossOrigin
	@PostMapping(value="/empresa/{id}/detalhes/{text}", consumes = "multipart/form-data")
	public NotasFiscais salvarArquivo(@PathVariable(name="id") Long id, @PathVariable(name="text") String text, 
			@RequestParam("arquivo") MultipartFile file) {
		NotasFiscais arquivo = new NotasFiscais();
		try {			
			arquivo.setContentType(file.getContentType());
			arquivo.setNomeArquivo(file.getOriginalFilename());
			arquivo.setConteudoArquivo(file.getBytes());
			arquivo.setGuidEmpresa(id);
			arquivo.setDetalhesNota(text);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return notasFiscaisService.save(arquivo);
	}
	
	@GetMapping(value="/find/{text}")
	public List<NotasFiscais> findByLike(@PathVariable(name="text") String text) {
		return notasFiscaisService.findByLike(text);
	}
	
}
