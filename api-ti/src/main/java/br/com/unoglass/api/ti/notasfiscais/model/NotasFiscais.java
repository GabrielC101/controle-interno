package br.com.unoglass.api.ti.notasfiscais.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="NOTASFISCAIS")
public class NotasFiscais {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long guidNota;
	private String nomeArquivo;
	@Lob
	private byte[] conteudoArquivo;
	private String contentType;
	private Long guidEquipamento;
	private String tokenIdentificacao;
	private String detalhesNota;
	private Long guidEmpresa;
	
	public String getDetalhesNota() {
		return detalhesNota;
	}
	public void setDetalhesNota(String detalhesNota) {
		this.detalhesNota = detalhesNota;
	}
	public Long getGuidEmpresa() {
		return guidEmpresa;
	}
	public void setGuidEmpresa(Long guidEmpresa) {
		this.guidEmpresa = guidEmpresa;
	}
	public String getTokenIdentificacao() {
		return tokenIdentificacao;
	}
	public void setTokenIdentificacao(String tokenIdentificacao) {
		this.tokenIdentificacao = tokenIdentificacao;
	}
	public Long getGuidEquipamento() {
		return guidEquipamento;
	}
	public void setGuidEquipamento(Long guidEquipamento) {
		this.guidEquipamento = guidEquipamento;
	}
	public Long getGuidNota() {
		return guidNota;
	}
	public void setGuidNota(Long guidNota) {
		this.guidNota = guidNota;
	}
	public String getNomeArquivo() {
		return nomeArquivo;
	}
	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
	public byte[] getConteudoArquivo() {
		return conteudoArquivo;
	}
	public void setConteudoArquivo(byte[] conteudoArquivo) {
		this.conteudoArquivo = conteudoArquivo;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

}
