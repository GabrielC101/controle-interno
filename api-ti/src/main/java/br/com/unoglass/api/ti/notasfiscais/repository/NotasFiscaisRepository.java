package br.com.unoglass.api.ti.notasfiscais.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.unoglass.api.ti.notasfiscais.model.NotasFiscais;

public interface NotasFiscaisRepository extends JpaRepository<NotasFiscais, Long>{

	@Query("select n from NotasFiscais n where n.guidEquipamento = ?1")
	NotasFiscais findByIdEquipamento(Long id);

	@Query("select n from NotasFiscais n where n.tokenIdentificacao = ?1")
	NotasFiscais findByTokenIdf(String token);

	@Query("select n from NotasFiscais n where n.guidEmpresa = ?1")
	List<NotasFiscais> findByEmpresa(Long id);

	@Query("select n from NotasFiscais n where n.detalhesNota LIKE %?1%")
	List<NotasFiscais> findByNotasLike(@Param("text") String text);

}
