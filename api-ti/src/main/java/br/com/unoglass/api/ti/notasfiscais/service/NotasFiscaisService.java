package br.com.unoglass.api.ti.notasfiscais.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.notasfiscais.model.NotasFiscais;
import br.com.unoglass.api.ti.notasfiscais.repository.NotasFiscaisRepository;

@Service
public class NotasFiscaisService {
	
	@Autowired
	NotasFiscaisRepository notasFiscaisRepository;
	
	public NotasFiscais findOneById(Long guidArquivo) {
		return notasFiscaisRepository.findById(guidArquivo).get();
	}

	public NotasFiscais save(NotasFiscais arquivo) {
		return notasFiscaisRepository.save(arquivo);
	}

	public NotasFiscais findByIdEquipamento(Long id) {
		return notasFiscaisRepository.findByIdEquipamento(id);
	}

	public void deleteNota(Long id) {
		notasFiscaisRepository.deleteById(id);
	}

	public NotasFiscais findByTokenIdf(String token) {
		return notasFiscaisRepository.findByTokenIdf(token);
	}

	public List<NotasFiscais> findByEmpresa(Long id) {
		return notasFiscaisRepository.findByEmpresa(id);
	}

	public List<NotasFiscais> findByLike(String text) {
		return notasFiscaisRepository.findByNotasLike(text);
	}
	
}
