package br.com.unoglass.api.ti.perfil.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unoglass.api.ti.perfil.model.Imagem;
import br.com.unoglass.api.ti.perfil.service.ImagemService;

@RestController
@RequestMapping(value="/api/auth")
public class DownloadController {

	@Autowired
	ImagemService imagemService;
	
	@GetMapping("/download/{id}")
	public ResponseEntity<Resource> download(@PathVariable Long id) {
		Imagem arquivo = imagemService.findBydUser(id);
		Resource resource = new ByteArrayResource(arquivo.getConteudoArquivo());
		String contentType = arquivo.getContentType();
		if (contentType == null) {
			contentType = "application/octet-stream";
		}
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + 
						arquivo.getNomeArquivo() + "\"")
				.body(resource);
	}
	
}
