package br.com.unoglass.api.ti.perfil.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.com.unoglass.api.ti.perfil.model.Imagem;
import br.com.unoglass.api.ti.perfil.service.ImagemService;

@RestController
@RequestMapping(value="/api/ti/perfil")
public class ImagemController {
	
	@Autowired
	ImagemService imagemService;
	
	@CrossOrigin
	@PreAuthorize("hasRole('ROLE_USER')")
	@PostMapping(value="/user/{id}", consumes = "multipart/form-data")
	public Imagem salvar(@PathVariable(name="id") Long id, @RequestParam("arquivo") MultipartFile file) {
		Imagem arquivo = new Imagem();
		try {			
			arquivo.setContentType(file.getContentType());
			arquivo.setNomeArquivo(file.getOriginalFilename());
			arquivo.setConteudoArquivo(file.getBytes());
			arquivo.setGuiUser(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		deletarUsuario(id);
		return imagemService.save(arquivo);
	}
	
	@GetMapping("/download/{id}")
	public ResponseEntity<Resource> download(@PathVariable Long id) {
		Imagem arquivo = imagemService.findBydUser(id);
		Resource resource = new ByteArrayResource(arquivo.getConteudoArquivo());
		String contentType = arquivo.getContentType();
		if (contentType == null) {
			contentType = "application/octet-stream";
		}
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + 
						arquivo.getNomeArquivo() + "\"")
				.body(resource);
	}
	
	@DeleteMapping(value="/delete/user/{id}")
	public void deletarUsuario(@PathVariable Long id) {
		imagemService.deletarUsuarioPorId(id);
	}
}
