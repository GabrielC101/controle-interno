package br.com.unoglass.api.ti.perfil.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="IMAGEMPERFIL")
public class Imagem {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long guidImagem;
	private String nomeArquivo;
	@Lob
	private byte[] conteudoArquivo;
	private String contentType;
	private Long guiUser;
	private Integer identificadorArquivo;
	
	public Integer getIdentificadorArquivo() {
		return identificadorArquivo;
	}
	public void setIdentificadorArquivo(Integer identificadorArquivo) {
		this.identificadorArquivo = identificadorArquivo;
	}
	public Long getGuidImagem() {
		return guidImagem;
	}
	public void setGuidImagem(Long guidImagem) {
		this.guidImagem = guidImagem;
	}
	public String getNomeArquivo() {
		return nomeArquivo;
	}
	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
	public byte[] getConteudoArquivo() {
		return conteudoArquivo;
	}
	public void setConteudoArquivo(byte[] conteudoArquivo) {
		this.conteudoArquivo = conteudoArquivo;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public Long getGuiUser() {
		return guiUser;
	}
	public void setGuiUser(Long guiUser) {
		this.guiUser = guiUser;
	}
	
}
