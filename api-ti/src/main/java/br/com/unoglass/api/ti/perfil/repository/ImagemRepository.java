package br.com.unoglass.api.ti.perfil.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import br.com.unoglass.api.ti.perfil.model.Imagem;

public interface ImagemRepository extends JpaRepository<Imagem, Long>{

	@Query("select i from Imagem i where i.guiUser = ?1")
	Imagem findBydUser(Long id);
	@Transactional
	@Modifying
	@Query("delete from Imagem i where i.guiUser = ?1")
	void deleteByGuiUser(Long id);

}
