package br.com.unoglass.api.ti.perfil.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.perfil.model.Imagem;
import br.com.unoglass.api.ti.perfil.repository.ImagemRepository;

@Service
public class ImagemService {

	@Autowired
	ImagemRepository imagemRepository;
	
	public Imagem findOneById(Long guidArquivo) {
		return imagemRepository.findById(guidArquivo).get();
	}

	public Imagem save(Imagem arquivo) {
		return imagemRepository.save(arquivo);
	}

	public Imagem findBydUser(Long id) {
		return imagemRepository.findBydUser(id);
	}

	public void deletarUsuarioPorId(Long id) {
		imagemRepository.deleteByGuiUser(id);
	}
	
}
