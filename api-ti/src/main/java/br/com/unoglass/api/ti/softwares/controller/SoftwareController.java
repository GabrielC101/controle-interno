package br.com.unoglass.api.ti.softwares.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unoglass.api.ti.softwares.model.Software;
import br.com.unoglass.api.ti.softwares.service.SoftwareService;

@RestController
@RequestMapping(value="/api/ti/software")
public class SoftwareController {

	@Autowired
	SoftwareService softwareService;
	
	@GetMapping
	public List<Software> listAll() {
		return softwareService.listarTodos();
	}
	
}
