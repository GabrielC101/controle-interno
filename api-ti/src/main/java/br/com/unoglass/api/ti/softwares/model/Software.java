package br.com.unoglass.api.ti.softwares.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SOFTWARE")
public class Software {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long guidSoftware;
	private String nomeSoftware;
	
	public Long getGuidSoftware() {
		return guidSoftware;
	}
	public void setGuidSoftware(Long guidSoftware) {
		this.guidSoftware = guidSoftware;
	}
	public String getNomeSoftware() {
		return nomeSoftware;
	}
	public void setNomeSoftware(String nomeSoftware) {
		this.nomeSoftware = nomeSoftware;
	}
	
}
