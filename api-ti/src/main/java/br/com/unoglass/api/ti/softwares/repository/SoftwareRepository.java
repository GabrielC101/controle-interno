package br.com.unoglass.api.ti.softwares.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.unoglass.api.ti.softwares.model.Software;

public interface SoftwareRepository extends JpaRepository<Software, Long>{

}
