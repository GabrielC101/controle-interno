package br.com.unoglass.api.ti.softwares.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.softwares.model.Software;
import br.com.unoglass.api.ti.softwares.repository.SoftwareRepository;
import br.com.unoglass.api.ti.util.BusinessException;
import br.com.unoglass.api.ti.util.BusinessExceptionCode;

@Service
public class SoftwareService {
	
	@Autowired
	SoftwareRepository softwareRepository;
	
	public List<Software> listarTodos() {
		try {
			return softwareRepository.findAll();
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR019);
		}
	}
	
}
