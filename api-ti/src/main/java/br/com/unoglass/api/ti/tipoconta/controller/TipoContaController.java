package br.com.unoglass.api.ti.tipoconta.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.unoglass.api.ti.tipoconta.model.TipoConta;
import br.com.unoglass.api.ti.tipoconta.service.TipoContaService;

@RestController
@RequestMapping(value="/api/ti/conta/tipo")
public class TipoContaController {

	@Autowired
	TipoContaService tipoContaService;
	
	@GetMapping
	public List<TipoConta> listAll() {
		return tipoContaService.listarTodos();
	}
	
}
