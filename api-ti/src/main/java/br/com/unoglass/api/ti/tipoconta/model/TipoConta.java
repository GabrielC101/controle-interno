package br.com.unoglass.api.ti.tipoconta.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="TIPOCONTA")
public class TipoConta {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long guidTipoConta;
	private String descricaoTipoConta;
	
	public Long getGuidTipoConta() {
		return guidTipoConta;
	}
	public void setGuidTipoConta(Long guidTipoConta) {
		this.guidTipoConta = guidTipoConta;
	}
	public String getDescricaoTipoConta() {
		return descricaoTipoConta;
	}
	public void setDescricaoTipoConta(String descricaoTipoConta) {
		this.descricaoTipoConta = descricaoTipoConta;
	}
	
}
