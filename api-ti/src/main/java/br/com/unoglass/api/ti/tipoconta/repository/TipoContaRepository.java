package br.com.unoglass.api.ti.tipoconta.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.unoglass.api.ti.tipoconta.model.TipoConta;

public interface TipoContaRepository extends JpaRepository<TipoConta, Long>{

}
