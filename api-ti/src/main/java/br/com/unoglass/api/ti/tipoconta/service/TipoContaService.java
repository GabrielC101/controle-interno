package br.com.unoglass.api.ti.tipoconta.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.unoglass.api.ti.tipoconta.model.TipoConta;
import br.com.unoglass.api.ti.tipoconta.repository.TipoContaRepository;
import br.com.unoglass.api.ti.util.BusinessException;
import br.com.unoglass.api.ti.util.BusinessExceptionCode;

@Service
public class TipoContaService {

	@Autowired
	TipoContaRepository tipoContaRepository;
	
	public List<TipoConta> listarTodos() {
		try {
			return tipoContaRepository.findAll();
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR013);
		}
	}
	
	public Optional<TipoConta> listarUm(Long id) {
		try {
			return tipoContaRepository.findById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR013);
		}
	}
	
	public TipoConta inserirUm(TipoConta tipoConta) {
		try {
			return tipoContaRepository.save(tipoConta);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR014);
		}
	}
	
	public void deletarUm(Long id) {
		try {
			tipoContaRepository.deleteById(id);
		}
		catch(Exception e) {
			throw new BusinessException(BusinessExceptionCode.ERR008);
		}
	}
	
}
