import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { PrincipalComponent } from './pages/patrimonio/principal/principal.component';
import { ComputadoresComponent } from './pages/patrimonio/computadores/computadores.component';
import { ColetoresComponent } from './pages/patrimonio/coletores/coletores.component';
import { EmpresaMenuComponent } from './pages/empresa/empresa-menu/empresa-menu.component';
import { EmpresaListarComponent } from './pages/empresa/empresa-listar/empresa-listar.component';
import { DepartamentoListarComponent } from './pages/empresa/departamento-listar/departamento-listar.component';
import { EmpresaCadastrarComponent } from './pages/empresa/empresa-cadastrar/empresa-cadastrar.component';
import { DepartamentoCadastrarComponent } from './pages/empresa/departamento-cadastrar/departamento-cadastrar.component';
import { ComputadoresCadastrarComponent } from './pages/patrimonio/computadores-cadastrar/computadores-cadastrar.component';
import { ColetoresCadastrarComponent } from './pages/patrimonio/coletores-cadastrar/coletores-cadastrar.component';
import { ManutencaoComponent } from './pages/patrimonio/manutencao/manutencao.component';
import { ManutencaoCadastrarComponent } from './pages/patrimonio/manutencao-cadastrar/manutencao-cadastrar.component';
import { ManutencaoColetorComponent } from './pages/patrimonio/manutencao-coletor/manutencao-coletor.component';
import { ManutencaoColetorCadastrarComponent } from './pages/patrimonio/manutencao-coletor-cadastrar/manutencao-coletor-cadastrar.component';
import { ContasAPagarListarComponent } from './pages/contas-a-pagar/contas-a-pagar-listar/contas-a-pagar-listar.component';
import { ContasAPagarCadastrarComponent } from './pages/contas-a-pagar/contas-a-pagar-cadastrar/contas-a-pagar-cadastrar.component';
import { NotasFiscaisListarComponent } from './pages/notas-fiscais/notas-fiscais-listar/notas-fiscais-listar.component';
import { NotasFiscaisCadastrarComponent } from './pages/notas-fiscais/notas-fiscais-cadastrar/notas-fiscais-cadastrar.component';
import { BackupListarComponent } from './pages/backup/backup-listar/backup-listar.component';
import { BackupCadastrarComponent } from './pages/backup/backup-cadastrar/backup-cadastrar.component';
import { CertificadoListarComponent } from './pages/certificado/certificado-listar/certificado-listar.component';
import { CertificadoCadastrarComponent } from './pages/certificado/certificado-cadastrar/certificado-cadastrar.component';
import { DominioListarComponent } from './pages/dominio/dominio-listar/dominio-listar.component';
import { DominioCadastrarComponent } from './pages/dominio/dominio-cadastrar/dominio-cadastrar.component';
import { EmailMenuComponent } from './pages/email/email-menu/email-menu.component';
import { EmailConfiguracaoComponent } from './pages/email/email-configuracao/email-configuracao.component';
import { EmailGrupoComponent } from './pages/email/email-grupo/email-grupo.component';
import { EmailContatoComponent } from './pages/email/email-contato/email-contato.component';
import { EmailContatoListaComponent } from './pages/email-contato-lista/email-contato-lista.component';
import { EmailTransmitirComponent } from './pages/email/email-transmitir/email-transmitir.component';
import { UsuarioAdministrarComponent } from './pages/usuario/usuario-administrar/usuario-administrar.component';
import { AuthGuard } from './auth.guard';

const routes: Routes =[
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  }, 

  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },

  {
    path: '',
    canActivate: [AuthGuard],
    component: AdminLayoutComponent,
    children: [
      { path: 'dashboard', component: DashboardComponent },
      { path: 'user-profile', component: UserProfileComponent },
      { path: 'principal', component: PrincipalComponent }, 
      { path: 'computadores', component: ComputadoresComponent },  
      { path: 'coletores', component: ColetoresComponent }, 
      { path: 'empresa-menu', component: EmpresaMenuComponent },
      { path: 'empresa', component: EmpresaListarComponent },
      { path: 'departamento', component: DepartamentoListarComponent },
      { path: 'departamento/novo', component: DepartamentoCadastrarComponent },
      { path: 'departamento/:id', component: DepartamentoCadastrarComponent },
      { path: 'empresa/:id', component: EmpresaCadastrarComponent },
      { path: 'empresa/novo', component: EmpresaCadastrarComponent },
      { path: 'computadores/novo', component: ComputadoresCadastrarComponent },
      { path: 'computadores/:id', component: ComputadoresCadastrarComponent },
      { path: 'coletores/novo', component: ColetoresCadastrarComponent },
      { path: 'coletores/:id', component: ColetoresCadastrarComponent },
      { path: 'manutencao/:id', component: ManutencaoComponent },
      { path: 'manutencao', component: ManutencaoCadastrarComponent },
      { path: 'manutencao/new/:id', component: ManutencaoCadastrarComponent },
      { path: 'manutencao/alterar/:id', component: ManutencaoCadastrarComponent },
      { path: 'manutencao-coletor/:id', component: ManutencaoColetorComponent },
      { path: 'manutencao-coletor/new/:id', component: ManutencaoColetorCadastrarComponent },
      { path: 'cpa', component: ContasAPagarListarComponent },
      { path: 'cpa-cadastrar/novo', component: ContasAPagarCadastrarComponent },
      { path: 'cpa-cadastrar/:id', component: ContasAPagarCadastrarComponent },
      { path: 'nfe', component: NotasFiscaisListarComponent },
      { path: 'nfe-cadastrar/novo', component: NotasFiscaisCadastrarComponent },
      { path: 'backups', component: BackupListarComponent },
      { path: 'backups-cadastrar/novo', component: BackupCadastrarComponent },
      { path: 'backups-cadastrar/:id', component: BackupCadastrarComponent },
      { path: 'certificados', component: CertificadoListarComponent },
      { path: 'certificados-cadastrar/novo', component: CertificadoCadastrarComponent },
      { path: 'dominios', component: DominioListarComponent },
      { path: 'dominios-cadastrar/novo', component: DominioCadastrarComponent },
      { path: 'dominios-cadastrar/:id', component: DominioCadastrarComponent },
      { path: 'malote', component: EmailMenuComponent },
      { path: 'email-config', component: EmailConfiguracaoComponent },
      { path: 'email-grupo', component: EmailGrupoComponent },
      { path: 'email-contato', component: EmailContatoComponent },
      { path: 'email-listar-contato/:id', component: EmailContatoListaComponent },
      { path: 'email-transmitir', component: EmailTransmitirComponent },
      { path: 'usuarios', component: UsuarioAdministrarComponent },
    ]
  },

  {
    path: '',
    component: AuthLayoutComponent,
    children: [
      { path: '', loadChildren: './layouts/auth-layout/auth-layout.module#AuthLayoutModule' },
    ]
  },

  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{
      useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
