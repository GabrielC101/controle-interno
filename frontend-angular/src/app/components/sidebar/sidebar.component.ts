import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/principal', title: 'Patrimônio',  icon: 'ni-building text-yellow', class: '' },
    { path: '/cpa', title: 'Contas a Pagar',  icon: 'ni-credit-card text-green', class: '' },
    { path: '/nfe', title: 'Notas Fiscais',  icon: 'ni-single-copy-04 text-grey', class: '' },
    { path: '/empresa-menu', title: 'Empresa',  icon: 'ni-istanbul text-purple', class: '' },
    { path: '/backups', title: 'Backups',  icon: 'ni-folder-17 text-red', class: '' },
    { path: '/certificados', title: 'Certificados',  icon: 'ni-badge text-cyan', class: '' },
    { path: '/dominios', title: 'Domínios',  icon: 'ni-calendar-grid-58 text-orange', class: '' },
    { path: '/usuarios', title: 'Usuários',  icon: 'ni-circle-08 text-blue', class: '' },
    { path: '/malote', title: 'Envio Malote',  icon: 'ni-email-83 text-pink', class: '' },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public isCollapsed = true;

  constructor(private router: Router) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/login'])
  }
  
}
