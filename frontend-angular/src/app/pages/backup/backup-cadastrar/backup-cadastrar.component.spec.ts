import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackupCadastrarComponent } from './backup-cadastrar.component';

describe('BackupCadastrarComponent', () => {
  let component: BackupCadastrarComponent;
  let fixture: ComponentFixture<BackupCadastrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackupCadastrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackupCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
