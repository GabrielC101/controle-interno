import { Component, OnInit } from '@angular/core';
import { Backup } from '../model/backup.model';
import { Empresa } from '../../empresa/model/empresa.model';
import { Software } from '../model/software.model';
import { EmpresaService } from '../../empresa/empresa.service';
import { BackupService } from '../backup.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-backup-cadastrar',
  templateUrl: './backup-cadastrar.component.html',
  styleUrls: ['./backup-cadastrar.component.scss']
})
export class BackupCadastrarComponent implements OnInit {

  constructor(private empresaService: EmpresaService,
              private backupService: BackupService,
              private toastr: ToastrService,
              private router: ActivatedRoute) { }

  backup: Backup = new Backup();
  empresas: Empresa[];
  softwares: Software[];
  selectedOption2: string;
  selectedOption: string;
  idEmpresa: number;
  idSoftware: number;
  _idParams: string;
  _idParamsInt: number;

  ngOnInit() {
    this.router.paramMap.subscribe(params => {
      if(params.get('id') != 'novo') {
        this._idParams = params.get('id');
        this._idParamsInt = Number.parseInt(this._idParams);
        this.backupService.loadBackupById(this._idParamsInt).subscribe(
          _backup => {
            this.backup = _backup;
            this.selectedOption2 = this.backup.guidEmpresa.toString();
            this.selectedOption = this.backup.guidSoftware.toString();
          }
        )
      }
    })
    this.loadEmpresas();
    this.loadSoftwares();
  }

  loadEmpresas() {
    this.empresaService.loadEmpresa().subscribe(
      _empresas => {
        this.empresas = _empresas;
      }
    )
  }

  loadSoftwares() {
    this.backupService.loadSoftwares().subscribe(
      _softwares => {
        this.softwares = _softwares
      }
    )
  }

  submitBackup() {
    if(this.selectedOption == undefined || this.selectedOption2 == undefined) {
      this.toastr.error('Campos obrigatórios estão nulos');
    }
    else {
      this.idEmpresa = Number.parseInt(this.selectedOption2);
      this.idSoftware = Number.parseInt(this.selectedOption);
      this.backup.guidEmpresa = this.idEmpresa;
      this.backup.guidSoftware = this.idSoftware;
      this.backupService.saveBackup(this.backup).subscribe(
        () => {
          this.toastr.success('Cadastrado com sucesso!');
        }
      )
    }
    setTimeout(()=> {
      window.location.reload();
    }, 1000)
  }

  deletar() {
    if(this.backup.guidBackup == null || this.backup.guidBackup == undefined) {
      this.toastr.error('Impossível deletar um registro que não foi criado!');
    }
    else {
      this.backupService.deleteBackup(this.backup.guidBackup).subscribe(
        () => {
          this.toastr.success('Registro deletado!');
        }
      )
      setTimeout(() => {
        window.location.reload();
      }, 1000)
    }
  }

}
