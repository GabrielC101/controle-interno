import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackupListarComponent } from './backup-listar.component';

describe('BackupListarComponent', () => {
  let component: BackupListarComponent;
  let fixture: ComponentFixture<BackupListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackupListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackupListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
