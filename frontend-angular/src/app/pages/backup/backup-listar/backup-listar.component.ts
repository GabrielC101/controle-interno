import { Component, OnInit } from '@angular/core';
import { BackupService } from '../backup.service';
import { EmpresaService } from '../../empresa/empresa.service';
import { Backup } from '../model/backup.model';
import { Empresa } from '../../empresa/model/empresa.model';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-backup-listar',
  templateUrl: './backup-listar.component.html',
  styleUrls: ['./backup-listar.component.scss']
})
export class BackupListarComponent implements OnInit {

  constructor(private backupService: BackupService,
              private empresaService: EmpresaService,
              private router: Router,
              private toastr: ToastrService) { }

  backups: Backup[];
  selectedOption2: string;
  idEmpresa: number;
  empresas: Empresa[];
  pageAtual: number = 1;

  ngOnInit() {
    this.loadEmpresas();
  }

  loadEmpresas() {
    this.empresaService.loadEmpresa().subscribe(
      _empresas => {
        this.empresas = _empresas
      }
    )
  }

  popularTableFiltro() {
    if(this.selectedOption2 == null || this.selectedOption2 == undefined) {
      this.toastr.error('Preencha o filtro!');
    }
    else {
      this.idEmpresa = Number.parseInt(this.selectedOption2);
      this.backupService.loadBackups(this.idEmpresa).subscribe(
        _backups => {
          this.backups = _backups;
        }
      )
    }
  }

  goToInserirBackup() {
    this.router.navigate(['/backups-cadastrar/novo']);
  }

  alterarDados(backup: Backup) {
    this.router.navigate(['/backups-cadastrar/'+backup.guidBackup]);
  }


}
