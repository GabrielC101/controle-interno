import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackupListarComponent } from './backup-listar/backup-listar.component';
import { BackupCadastrarComponent } from './backup-cadastrar/backup-cadastrar.component';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [BackupListarComponent, BackupCadastrarComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule
  ]
})
export class BackupModule { }
