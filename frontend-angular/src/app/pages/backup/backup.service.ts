import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Backup } from './model/backup.model';
import { Software } from './model/software.model';

const urlApiBackupsByEmpresa = 'http://localhost:8080/api/ti/backup/empresa/';
const urlApiBackups = 'http://localhost:8080/api/ti/backup';
const urlApiSoftware = 'http://localhost:8080/api/ti/software';

@Injectable({
  providedIn: 'root'
})
export class BackupService {

  constructor(private http: HttpClient) { }

  loadBackups(id: number): Observable<Backup[]> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.get<Backup[]>(urlApiBackupsByEmpresa+id, options);
  }

  loadBackupById(id: number): Observable<Backup> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.get<Backup>(urlApiBackups+'/'+id, options);
  }

  loadSoftwares(): Observable<Software[]> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.get<Software[]>(urlApiSoftware, options);
  }

  saveBackup(backup: Backup): Observable<Backup> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.post<Backup>(urlApiBackups, backup, options);
  }

  deleteBackup(id: number): Observable<any> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.delete<Backup>(urlApiBackups+'/'+id, options);
  }

}
