export class Backup {
    guidBackup: number;
    caminhoBackup: string;
    guidEmpresa: number;
    nomeBackup: string;
    softwareNome: string;
    guidSoftware: number;
    senhaRede: string;
    usuarioRede: string;
}