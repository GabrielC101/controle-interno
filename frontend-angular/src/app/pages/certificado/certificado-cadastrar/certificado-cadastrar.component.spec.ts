import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificadoCadastrarComponent } from './certificado-cadastrar.component';

describe('CertificadoCadastrarComponent', () => {
  let component: CertificadoCadastrarComponent;
  let fixture: ComponentFixture<CertificadoCadastrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificadoCadastrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificadoCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
