import { Component, OnInit } from '@angular/core';
import { Certificado } from '../model/certificado.model';
import { Empresa } from '../../empresa/model/empresa.model';
import { CertificadoService } from '../certificado.service';
import { EmpresaService } from '../../empresa/empresa.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-certificado-cadastrar',
  templateUrl: './certificado-cadastrar.component.html',
  styleUrls: ['./certificado-cadastrar.component.scss']
})
export class CertificadoCadastrarComponent implements OnInit {

  constructor(private certificadoService: CertificadoService,
              private empresaService: EmpresaService,
              private toastr: ToastrService) { }

  certificado: Certificado = new Certificado();
  empresas: Empresa[];
  selectedFiles: FileList;
  currentFileUpload: File;
  selectedOption2: string;
  _idNumber: number;

  ngOnInit() {
    this.loadEmpresas();
  }

  loadEmpresas() {
    this.empresaService.loadEmpresa().subscribe(
      _empresas => {
        this.empresas = _empresas;
      }
    )
  }

  submitCertificados() {
    if(this.selectedFiles == undefined || this.selectedOption2 == undefined 
      || this.certificado.nomeCertificado == undefined || this.certificado.senhaCertificado == undefined) {
      this.toastr.error("Campos obrigatórios estão nulos!");
    }
    else {
      this._idNumber = Number.parseInt(this.selectedOption2);
      this.currentFileUpload = this.selectedFiles.item(0);
      this.certificadoService.salvar(this.currentFileUpload, this._idNumber, this.certificado.senhaCertificado, this.certificado.nomeCertificado).subscribe(
        () => {
          this.toastr.success('Certificado cadastrado com sucesso!');
        }
      )
      setTimeout(() => {
        window.location.reload();
      }, 1000)
    }
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }



}
