import { Component, OnInit } from '@angular/core';
import { EmpresaService } from '../../empresa/empresa.service';
import { Empresa } from '../../empresa/model/empresa.model';
import { CertificadoService } from '../certificado.service';
import { ToastrService } from 'ngx-toastr';
import { Certificado } from '../model/certificado.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-certificado-listar',
  templateUrl: './certificado-listar.component.html',
  styleUrls: ['./certificado-listar.component.scss']
})
export class CertificadoListarComponent implements OnInit {

  constructor(private empresaService: EmpresaService,
              private certificadoService: CertificadoService,
              private toastr: ToastrService,
              private router: Router) { }

  empresas: Empresa[];
  selectedOption2: string;
  _idEmpresa: number;
  certificados: Certificado[];
  pageAtual: number = 1;

  ngOnInit() {
    this.loadEmpresas();
  }

  popularTableFiltro() {
    if(this.selectedOption2 == undefined) {
      this.toastr.error("Preencha o filtro!");
    }
    else {
      this._idEmpresa = Number.parseInt(this.selectedOption2);
      this.certificadoService.loadCertificados(this._idEmpresa).subscribe(
        _certificados => {
          this.certificados = _certificados;
        }
      )
    }
  }

  loadEmpresas() {
    this.empresaService.loadEmpresa().subscribe(
      _empresas => {
        this.empresas = _empresas;
      }
    )
  }

  goToInserirCertificado() {
    this.router.navigate(['/certificados-cadastrar/novo']);
  }

  download(certificado: Certificado) {
    this.certificadoService.download(certificado.guidCertificado).subscribe(
      () => {

      }
    )
  }

  deletarCertificado(certificado: Certificado) {
    this.certificadoService.deleteById(certificado.guidCertificado).subscribe(
      () => {
        this.toastr.success('Registro deletado!')
      }
    )
    setTimeout(() => {
      window.location.reload();
    }, 1000)
  }

}
