import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CertificadoCadastrarComponent } from './certificado-cadastrar/certificado-cadastrar.component';
import { CertificadoListarComponent } from './certificado-listar/certificado-listar.component';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [CertificadoCadastrarComponent, CertificadoListarComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule
  ]
})
export class CertificadoModule { }
