import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Certificado } from './model/certificado.model';

const urlApiCertificado = 'http://localhost:8080/api/ti/certificado/empresa/';
const urlApiCertificadoSalvar = 'http://localhost:8080/api/ti/certificado/empresa/';
const urlApiCertificadoDownload = 'http://localhost:8080/api/ti/certificado/download/';
const urlApiDeletarDownload = 'http://localhost:8080/api/ti/certificado/';

@Injectable({
  providedIn: 'root'
})
export class CertificadoService {

  constructor(private http: HttpClient) { }

  loadCertificados(id: number): Observable<Certificado[]> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.get<Certificado[]>(urlApiCertificado+id, options);
  }

  salvar(data: File, id: number, pass: string, name: string): Observable<any> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    let formData = new FormData();
    formData.append('arquivo', data, data.name);
    return this.http.post<any>
      (urlApiCertificadoSalvar+id+'/password/'+pass+'/name/'+name, formData, options);
  }

  download(id: number): Observable<any> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.get<any>(urlApiCertificadoDownload+id, options);
  }

  deleteById(id: number): Observable<any> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.delete<Certificado>(urlApiDeletarDownload+id, options);
  }

}
