export class Certificado {
    guidCertificado: number;
    contentType: string;
    conteudoArquivo: any;
    nomeArquivo: string;
    nomeCertificado: string;
    senhaCertificado: string;
    guidEmpresa: number;
}