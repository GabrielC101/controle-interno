import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContasAPagarCadastrarComponent } from './contas-a-pagar-cadastrar.component';

describe('ContasAPagarCadastrarComponent', () => {
  let component: ContasAPagarCadastrarComponent;
  let fixture: ComponentFixture<ContasAPagarCadastrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContasAPagarCadastrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContasAPagarCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
