import { Component, OnInit } from '@angular/core';
import { EmpresaService } from '../../empresa/empresa.service';
import { TipoConta } from '../model/tipoconta.model';
import { ContasAPagarService } from '../contas-a-pagar.service';
import { Contas } from '../model/conta.model';
import { Empresa } from '../../empresa/model/empresa.model';
import { Toast, ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-contas-a-pagar-cadastrar',
  templateUrl: './contas-a-pagar-cadastrar.component.html',
  styleUrls: ['./contas-a-pagar-cadastrar.component.scss']
})
export class ContasAPagarCadastrarComponent implements OnInit {

  constructor(private empresaService: EmpresaService,
              private contasService: ContasAPagarService,
              private toastr: ToastrService,
              private router: ActivatedRoute,
              private route: Router) { }

  conta: Contas = new Contas();
  empresas: Empresa[];
  tipoContas: TipoConta[];
  selectedOption: string;
  selectedOption2: string;
  idEmpresas: number;
  idTipoContas: number;
  _parametroId: string;
  _parametroNumber: number;
  public myAngularxQrCode: string = null;

  ngOnInit() {
    this.router.paramMap.subscribe(
      params => {
        if(params.get('id') != 'novo') {
          this._parametroId = params.get('id');
          this._parametroNumber = Number.parseInt(this._parametroId);
          this.contasService.loadById(this._parametroNumber).subscribe(
            conta => {
              this.conta = conta;
              this.selectedOption2 = conta.guidEmpresa.toString();
              this.selectedOption = conta.guidTipoConta.toString();
              this.myAngularxQrCode = conta.siteConta;
            }
          )
        }
      }
    )
    this.loadEmpresas();
    this.loadTipoContas();
  }

  loadEmpresas() {
    this.empresaService.loadEmpresa().subscribe(
      empresas => {
        this.empresas = empresas;
      }
    )
  }

  loadTipoContas() {
    this.contasService.loadTipoConta().subscribe(
      contas => {
        this.tipoContas = contas;
      }
    )
  }

  submitContas() {
    this.idEmpresas = Number.parseInt(this.selectedOption2);
    this.idTipoContas = Number.parseInt(this.selectedOption);
    this.conta.guidEmpresa = this.idEmpresas;
    this.conta.guidTipoConta = this.idTipoContas;
    this.contasService.saveContas(this.conta).subscribe(
      () => {
        this.toastr.success("Cadastrado com sucesso!");
      }
    );
    setTimeout(() => {
      window.location.reload();
    }, 1000)
  }

  deletar() {
    if(this.conta.nomeConta == undefined) {
      this.toastr.error('Não há como deletar um registro que não foi inserido!')
    }
    else {
      this.contasService.deleteConta(this._parametroNumber).subscribe(
        () => {
          this.toastr.success("Registro deletado!")
        }
      ),
      setTimeout(() => {
        this.route.navigate(['/cpa']);
      }, 1000)
    }
  }

}
