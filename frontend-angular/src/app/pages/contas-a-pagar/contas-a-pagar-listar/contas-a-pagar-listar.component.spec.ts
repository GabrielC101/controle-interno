import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContasAPagarListarComponent } from './contas-a-pagar-listar.component';

describe('ContasAPagarListarComponent', () => {
  let component: ContasAPagarListarComponent;
  let fixture: ComponentFixture<ContasAPagarListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContasAPagarListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContasAPagarListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
