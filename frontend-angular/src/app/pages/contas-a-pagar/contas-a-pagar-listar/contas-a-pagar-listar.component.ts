import { Component, OnInit } from '@angular/core';
import { EmpresaService } from '../../empresa/empresa.service';
import { DepartamentoService } from '../../empresa/departamento.service';
import { Departamento } from '../../empresa/model/departamento.model';
import { Empresa } from '../../empresa/model/empresa.model';
import { Contas } from '../model/conta.model';
import { ToastrService } from 'ngx-toastr';
import { ContasAPagarService } from '../contas-a-pagar.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contas-a-pagar-listar',
  templateUrl: './contas-a-pagar-listar.component.html',
  styleUrls: ['./contas-a-pagar-listar.component.scss']
})
export class ContasAPagarListarComponent implements OnInit {

  constructor(private empresaService: EmpresaService,
              private toastr: ToastrService,
              private contasService: ContasAPagarService,
              private route: Router) { }

  empresas: Empresa[];
  contas: Contas[];
  selectedOption: string;
  selectedOption2: string;
  guiDepartamento: number;
  guidEmpresa: number;
  pageAtual: number = 1;

  ngOnInit() {
    this.loadEmpresa();
  }

  loadEmpresa() {
    this.empresaService.loadEmpresa().subscribe(
      empresa => {
        this.empresas = empresa;
      }
    )
  }

  popularTableFiltro() {
    if(this.selectedOption2 == undefined) {
      this.toastr.error("Preencha o filtro");
    }
    else {
      this.guidEmpresa = Number.parseInt(this.selectedOption2);
      this.contasService.loadContas(this.guidEmpresa).subscribe(
        contas => {
          this.contas = contas;
        }
      )
    }
  }

  goToInserirContas() {
    this.route.navigate(['/cpa-cadastrar/novo']);
  }

  alterarDados(conta: Contas) {
    this.route.navigate(['/cpa-cadastrar/'+conta.guidContas]);
  }

}
