import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContasAPagarListarComponent } from './contas-a-pagar-listar/contas-a-pagar-listar.component';
import { FormsModule } from '@angular/forms';
import { ContasAPagarCadastrarComponent } from './contas-a-pagar-cadastrar/contas-a-pagar-cadastrar.component';
import { QRCodeModule } from 'angular2-qrcode';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [ContasAPagarListarComponent, ContasAPagarCadastrarComponent],
  imports: [
    CommonModule,
    FormsModule,
    QRCodeModule,
    NgxPaginationModule
  ]
})
export class ContasAPagarModule { }
