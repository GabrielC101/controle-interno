import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Contas } from './model/conta.model';
import { TipoConta } from './model/tipoconta.model';

const urlApiContas = 'http://localhost:8080/api/ti/contasapagar/empresa/';
const urlApiContasSalvar = 'http://localhost:8080/api/ti/contasapagar/';
const urlApiTipoContas = 'http://localhost:8080/api/ti/conta/tipo';

@Injectable({
  providedIn: 'root'
})
export class ContasAPagarService {

  constructor(private http: HttpClient) { }

  loadContas(id: number): Observable<Contas[]> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.get<Contas[]>(urlApiContas+id, options);
  }

  loadTipoConta(): Observable<TipoConta[]> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.get<TipoConta[]>(urlApiTipoContas, options);
  }

  saveContas(contas: Contas): Observable<Contas> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.post<Contas>(urlApiContasSalvar, contas, options);
  }

  loadById(id: number): Observable<Contas> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.get<Contas>(urlApiContasSalvar+id, options);
  }

  deleteConta(id: number): Observable<any> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.delete<Contas>(urlApiContasSalvar+id, options);
  }

}
