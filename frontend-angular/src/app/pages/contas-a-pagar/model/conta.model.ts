export class Contas {
    guidContas: number;
    nomeConta: string;
    siteConta: string;
    indentificadorMensal: string;
    loginConta: string;
    passwordConta: string;
    guidTipoConta: number;
    guidEmpresa: number;
    dataVencimento: Date;
}