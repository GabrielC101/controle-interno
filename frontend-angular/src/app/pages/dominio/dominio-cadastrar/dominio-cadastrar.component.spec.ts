import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DominioCadastrarComponent } from './dominio-cadastrar.component';

describe('DominioCadastrarComponent', () => {
  let component: DominioCadastrarComponent;
  let fixture: ComponentFixture<DominioCadastrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DominioCadastrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DominioCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
