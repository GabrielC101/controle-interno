import { Component, OnInit } from '@angular/core';
import { DominioService } from '../dominio.service';
import { ToastrService } from 'ngx-toastr';
import { EmpresaService } from '../../empresa/empresa.service';
import { Empresa } from '../../empresa/model/empresa.model';
import { Dominio } from '../model/dominio.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-dominio-cadastrar',
  templateUrl: './dominio-cadastrar.component.html',
  styleUrls: ['./dominio-cadastrar.component.scss']
})
export class DominioCadastrarComponent implements OnInit {

  constructor(private dominioService: DominioService,
              private toastr: ToastrService,
              private empresaService: EmpresaService,
              private router: ActivatedRoute) { }

  empresas: Empresa[];
  dominio: Dominio = new Dominio();
  selectedOption2: string;
  _idNumber: number;
  _idParamsNumber: number;
  _idParamsString: string;
      
  ngOnInit() {
    this.router.paramMap.subscribe(params => {
      if(params.get('id') != 'novo') {
        this._idParamsString = params.get('id');
        this._idParamsNumber = Number.parseInt(this._idParamsString);
        this.dominioService.findById(this._idParamsNumber).subscribe(
          _dominio => {
            this.dominio = _dominio;
            this.selectedOption2 = this.dominio.guidEmpresa.toString();
          }
        )
      }
    })
    this.loadEmpresas();
  }

  loadEmpresas() {
    this.empresaService.loadEmpresa().subscribe(
      _empresas => {
        this.empresas = _empresas;
      }
    )
  }

  submitDominio() {
    if(this.dominio.linkFTP == undefined || this.dominio.passwordFTP == undefined 
      || this.dominio.urlDominio == undefined || this.dominio.usuarioFTP == undefined || this.selectedOption2 == undefined) {
        this.toastr.error("Campos obrigatórios estão nulos");
    }
    else {
      this._idNumber = Number.parseInt(this.selectedOption2);
      this.dominio.guidEmpresa = this._idNumber;
      this.dominioService.saveDominio(this.dominio).subscribe(
        () => {
          this.toastr.success('Cadastro inserido com sucesso!');
        }
      )
      setTimeout(() => {
        window.location.reload();
      }, 1000)
    }
  }

  deletar() {
    if(this.dominio.guidDominio == undefined || this.dominio.guidDominio == null) {
      this.toastr.error('Impossível remover um registro que não foi inserido!');
    }
    else {
      this.dominioService.deletarDominio(this.dominio.guidDominio).subscribe(
        () => {
          this.toastr.success('Registro deletado com sucesso!');
        }
      )
      setTimeout(() => {
        window.location.reload();
      }, 1000)
    }
  }

}
