import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DominioListarComponent } from './dominio-listar.component';

describe('DominioListarComponent', () => {
  let component: DominioListarComponent;
  let fixture: ComponentFixture<DominioListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DominioListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DominioListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
