import { Component, OnInit } from '@angular/core';
import { DominioService } from '../dominio.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Empresa } from '../../empresa/model/empresa.model';
import { Dominio } from '../model/dominio.model';
import { EmpresaService } from '../../empresa/empresa.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-dominio-listar',
  templateUrl: './dominio-listar.component.html',
  styleUrls: ['./dominio-listar.component.scss']
})
export class DominioListarComponent implements OnInit {

  constructor(private dominioService: DominioService,
              private toastr: ToastrService,
              private route: Router,
              private empresaService: EmpresaService) { }

  selectedOption2: string;
  empresas: Empresa[];
  dominios: Dominio[];
  _idNumber: number;
  pageAtual: number = 1;

  ngOnInit() {
    this.loadEmpresas();
  }

  popularTableFiltro() {
    this._idNumber = Number.parseInt(this.selectedOption2);
    this.dominioService.loadDominios(this._idNumber).subscribe(
      _dominios => {
        this.dominios = _dominios;
      }
    )
  }

  loadEmpresas() {
    this.empresaService.loadEmpresa().subscribe(
      _empresas => {
        this.empresas = _empresas;
      }
    )
  }

  goToInserirDominio() {
    this.route.navigate(['/dominios-cadastrar/novo']);
  }

  alterarDados(dominio: Dominio) {
    this.route.navigate(['/dominios-cadastrar/'+dominio.guidDominio]);
  }

}
