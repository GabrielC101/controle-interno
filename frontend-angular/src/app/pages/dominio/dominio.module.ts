import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DominioListarComponent } from './dominio-listar/dominio-listar.component';
import { DominioCadastrarComponent } from './dominio-cadastrar/dominio-cadastrar.component';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [DominioListarComponent, DominioCadastrarComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule
  ]
})
export class DominioModule { }
