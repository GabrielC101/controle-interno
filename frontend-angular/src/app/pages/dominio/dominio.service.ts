import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Dominio } from './model/dominio.model';
import { Observable } from 'rxjs';

const urlApiDominio = 'http://localhost:8080/api/ti/dominio/empresa/';
const urlApiSaveDominio = 'http://localhost:8080/api/ti/dominio/';

@Injectable({
  providedIn: 'root'
})
export class DominioService {

  constructor(private http: HttpClient) { }

  loadDominios(id: number): Observable<Dominio[]> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    }
    return this.http.get<Dominio[]>(urlApiDominio+id, options);
  }

  saveDominio(dominio: Dominio): Observable<Dominio> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    }
    return this.http.post<Dominio>(urlApiSaveDominio, dominio, options);
  }

  deletarDominio(id: number): Observable<any> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    }
    return this.http.delete<Dominio>(urlApiSaveDominio+id, options)
  }

  findById(id: number): Observable<Dominio> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    }
    return this.http.get<Dominio>(urlApiSaveDominio+id, options);
  }

}
