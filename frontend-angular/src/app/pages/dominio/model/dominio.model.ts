export class Dominio {
    guidDominio: number;
    linkFTP: string;
    passwordFTP: string;
    urlDominio: string;
    urlSSLDominio: string;
    usuarioFTP: string;
    guidEmpresa: number;
}