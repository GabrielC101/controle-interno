import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailContatoListaComponent } from './email-contato-lista.component';

describe('EmailContatoListaComponent', () => {
  let component: EmailContatoListaComponent;
  let fixture: ComponentFixture<EmailContatoListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailContatoListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailContatoListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
