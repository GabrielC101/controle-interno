import { Component, OnInit } from '@angular/core';
import { Contato } from '../email/model/contato.model';
import { ContatoService } from '../email/contato.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-email-contato-lista',
  templateUrl: './email-contato-lista.component.html',
  styleUrls: ['./email-contato-lista.component.scss']
})
export class EmailContatoListaComponent implements OnInit {

  constructor(private contatoService: ContatoService,
              private router: ActivatedRoute,
              private toastr: ToastrService) { }

  contato: Contato = new Contato();
  contatos: Contato[];
  _idString: string;
  _idNumber: number;
  pageAtual: number = 1;

  ngOnInit() {
    this.router.paramMap.subscribe(
      params => {
        this._idString = params.get('id');
        this._idNumber = Number.parseInt(this._idString);
      }
    )
    this.loadContatos();
  }

  loadContatos() {
    this.contatoService.loadContatosByGrupo(this._idNumber).subscribe(
      _contatos => {
        this.contatos = _contatos;
      }
    )
  }

  submitContato() {
    if(this.contato.enderecoEmail == undefined || this.contato.nomeContato == undefined) {
      this.toastr.error('Verifique se os campos estão preenchidos!');
    }
    else {
      this.contato.guidGrupo = this._idNumber;
      this.contatoService.saveContato(this.contato).subscribe(
        () => {
          this.toastr.success('Cadastrado com sucesso!');
        }
      )
      setTimeout(()=> {
        window.location.reload();
      }, 1000)
    }
  }

  deletar(contato: Contato) {
    this.contatoService.deleteContato(contato.guidContato).subscribe(
      () => {
        this.toastr.success('Registro deletado!');
      }
    )
    setTimeout(()=> {
      window.location.reload();
    }, 1000)
  }

}
