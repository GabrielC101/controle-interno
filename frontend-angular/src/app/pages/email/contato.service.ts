import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Contato } from './model/contato.model';

const urlApiContato = 'http://localhost:8080/api/ti/contato/grupo/';
const urlApiContatoSaveOrList = 'http://localhost:8080/api/ti/contato/';

@Injectable({
  providedIn: 'root'
})
export class ContatoService {

  constructor(private http: HttpClient) { }

  loadContatosByGrupo(id: number): Observable<Contato[]> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    }
    return this.http.get<Contato[]>(urlApiContato+id, options);
  }

  saveContato(contato: Contato): Observable<Contato> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    }
    return this.http.post<Contato>(urlApiContatoSaveOrList, contato, options);
  }

  deleteContato(id: number): Observable<any> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    }
    return this.http.delete<Contato>(urlApiContatoSaveOrList+id, options);
  }

}
