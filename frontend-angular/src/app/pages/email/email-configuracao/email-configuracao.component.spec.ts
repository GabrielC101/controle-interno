import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailConfiguracaoComponent } from './email-configuracao.component';

describe('EmailConfiguracaoComponent', () => {
  let component: EmailConfiguracaoComponent;
  let fixture: ComponentFixture<EmailConfiguracaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailConfiguracaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailConfiguracaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
