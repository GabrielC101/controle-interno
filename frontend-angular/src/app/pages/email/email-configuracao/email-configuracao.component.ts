import { Component, OnInit } from '@angular/core';
import { ConfigEmail } from '../model/config.model';
import { ActivatedRoute } from '@angular/router';
import { EmailService } from '../email.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-email-configuracao',
  templateUrl: './email-configuracao.component.html',
  styleUrls: ['./email-configuracao.component.scss']
})
export class EmailConfiguracaoComponent implements OnInit {

  constructor(private emailService: EmailService,
              private toastr: ToastrService) { }

  configEmail: ConfigEmail = new ConfigEmail();

  ngOnInit() {
    this.loadConfiguração();
  }

  loadConfiguração() {
    this.emailService.loadConfiguracao().subscribe(
      _config => {
        this.configEmail = _config;
      }
    )
  }

  submitConfig() {
    if(this.configEmail.emailAddress == undefined || this.configEmail.hostEmail == undefined ||
        this.configEmail.passwordEmail == undefined || this.configEmail.portEmail == undefined ||
          this.configEmail.smtpPort == undefined) {
      this.toastr.error('Campos obrigatórios estão nulos!');
    }
    else {
      this.emailService.saveConfig(this.configEmail).subscribe(
        () => {
          this.toastr.success('Cadastrado com sucesso!');
        }
      )
      setTimeout(() => {
        window.location.reload();
      }, 3000)
    }
  }

}
