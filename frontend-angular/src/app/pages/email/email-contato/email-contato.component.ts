import { Component, OnInit } from '@angular/core';
import { GrupoService } from '../grupo.service';
import { Grupo } from '../model/grupo.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-email-contato',
  templateUrl: './email-contato.component.html',
  styleUrls: ['./email-contato.component.scss']
})
export class EmailContatoComponent implements OnInit {

  constructor(private grupoService: GrupoService,
              private router: Router) { }

  grupos: Grupo[];
  pageAtual: number = 1;

  ngOnInit() {
    this.loadGrupos();
  }

  loadGrupos() {
    this.grupoService.loadGrupos().subscribe(
      _grupos => {
        this.grupos = _grupos;
      }
    )
  }

  alterar(grupo: Grupo) {
    this.router.navigate(['/email-listar-contato/'+grupo.guidGrupo]);
  }

}
