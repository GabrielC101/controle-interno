import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailGrupoComponent } from './email-grupo.component';

describe('EmailGrupoComponent', () => {
  let component: EmailGrupoComponent;
  let fixture: ComponentFixture<EmailGrupoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailGrupoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailGrupoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
