import { Component, OnInit } from '@angular/core';
import { GrupoService } from '../grupo.service';
import { Grupo } from '../model/grupo.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-email-grupo',
  templateUrl: './email-grupo.component.html',
  styleUrls: ['./email-grupo.component.scss']
})
export class EmailGrupoComponent implements OnInit {

  constructor(private grupoService: GrupoService,
              private toastr: ToastrService) { }

  grupos: Grupo[];
  grupo: Grupo = new Grupo();
  pageAtual: number = 1;

  ngOnInit() {
    this.loadGrupos();
  }

  loadGrupos() {
    this.grupoService.loadGrupos().subscribe(
      _grupos => {
        this.grupos = _grupos;
      }
    )
  }

  submitGrupo() {
    if(this.grupo.descricaoGrupo == undefined) {
      this.toastr.error('Insira o nome do grupo.')
    }
    else {
      this.grupoService.saveGrupo(this.grupo).subscribe(
        () => {
          this.toastr.success('Cadastrado com sucesso!')
        }
      )
      setTimeout(()=> {
        window.location.reload();
      }, 3000)
    }
  }

  deletar(grupo: Grupo) {
    this.grupoService.deleteGrupo(grupo.guidGrupo).subscribe(
      () => {
        this.toastr.success('Registro deletado!');
      }
    )
    setTimeout(()=> {
      window.location.reload();
    }, 3000)
  }

}
