import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-email-menu',
  templateUrl: './email-menu.component.html',
  styleUrls: ['./email-menu.component.scss']
})
export class EmailMenuComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToConfiguracao() {
    this.router.navigate(['/email-config']);
  }

  goToGrupo() {
    this.router.navigate(['/email-grupo']);
  }

  goToContatos() {
    this.router.navigate(['/email-contato']);
  }

  goToTransmitir() {
    this.router.navigate(['/email-transmitir']);
  }

}
