import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailTransmitirComponent } from './email-transmitir.component';

describe('EmailTransmitirComponent', () => {
  let component: EmailTransmitirComponent;
  let fixture: ComponentFixture<EmailTransmitirComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailTransmitirComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailTransmitirComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
