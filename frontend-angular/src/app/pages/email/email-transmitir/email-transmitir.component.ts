import { Component, OnInit } from '@angular/core';
import { GrupoService } from '../grupo.service';
import { Grupo } from '../model/grupo.model';
import { ToastrService } from 'ngx-toastr';
import { Mensagem } from '../model/email.model';
import { EmailService } from '../email.service';

@Component({
  selector: 'app-email-transmitir',
  templateUrl: './email-transmitir.component.html',
  styleUrls: ['./email-transmitir.component.scss']
})
export class EmailTransmitirComponent implements OnInit {

  constructor(private grupoService: GrupoService,
              private toastr: ToastrService,
              private emailService: EmailService) { }

  grupos: Grupo[];
  selectedOption2: number;
  mensagem: Mensagem = new Mensagem();

  ngOnInit() {
    this.loadGrupos();
  }

  loadGrupos() {
    this.grupoService.loadGrupos().subscribe(
      _grupos => {
        this.grupos = _grupos;
      }
    )
  }

  submitMensagem() {
    if(this.selectedOption2 == undefined || this.mensagem.mensagemWeb == undefined) {
      this.toastr.error('Selecione o grupo de envio.');
    }
    else {
      this.mensagem.guidGrupo = this.selectedOption2;
      this.emailService.exportMensagem(this.mensagem).subscribe(
        () => {
          this.toastr.success('Mensagem enviada com sucesso!')
        }
      )
      /*setTimeout(()=> {
        window.location.reload();
      }, 3000)*/
    }
  }



}
