import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailMenuComponent } from './email-menu/email-menu.component';
import { EmailConfiguracaoComponent } from './email-configuracao/email-configuracao.component';
import { FormsModule } from '@angular/forms';
import { EmailGrupoComponent } from './email-grupo/email-grupo.component';
import { EmailContatoComponent } from './email-contato/email-contato.component';
import { EmailTransmitirComponent } from './email-transmitir/email-transmitir.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { EmailContatoListaComponent } from '../email-contato-lista/email-contato-lista.component';

@NgModule({
  declarations: [EmailMenuComponent, EmailConfiguracaoComponent, EmailGrupoComponent, EmailContatoComponent, EmailTransmitirComponent, EmailContatoListaComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule
  ]
})
export class EmailModule { }
