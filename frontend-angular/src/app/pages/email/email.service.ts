import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConfigEmail } from './model/config.model';
import { Mensagem } from './model/email.model';

const urlApiConfiguracao = 'http://localhost:8080/api/ti/config/email';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor(private http: HttpClient) { }

  loadConfiguracao(): Observable<ConfigEmail> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    }
    return this.http.get<ConfigEmail>(urlApiConfiguracao, options);
  }

  saveConfig(config: ConfigEmail): Observable<ConfigEmail> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    }
    return this.http.post<ConfigEmail>(urlApiConfiguracao, config, options);
  }

  exportMensagem(mensagem: Mensagem): Observable<Mensagem> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    }
    return this.http.post<Mensagem>(urlApiConfiguracao+'/mensagem', mensagem, options);
  }

}
