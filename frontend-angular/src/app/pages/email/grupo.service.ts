import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Grupo } from './model/grupo.model';

const urlApiGrupo = 'http://localhost:8080/api/ti/grupo/';

@Injectable({
  providedIn: 'root'
})
export class GrupoService {

  constructor(private http: HttpClient) { }

  loadGrupos(): Observable<Grupo[]> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    }
    return this.http.get<Grupo[]>(urlApiGrupo, options);
  }

  saveGrupo(grupo: Grupo): Observable<Grupo> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    }
    return this.http.post<Grupo>(urlApiGrupo, grupo, options);
  }

  deleteGrupo(id: number): Observable<any> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    }
    return this.http.delete<Grupo>(urlApiGrupo+id, options);
  }

}
