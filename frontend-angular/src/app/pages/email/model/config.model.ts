export class ConfigEmail {
    guidEmailConfig: number;
    hostEmail: string;
    portEmail: string;
    smtpPort: string;
    emailAddress: string;
    passwordEmail: string;
}