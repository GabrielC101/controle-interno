export class Contato {
    guidContato: number;
    guidGrupo: number;
    nomeContato: string;
    enderecoEmail: string;
}