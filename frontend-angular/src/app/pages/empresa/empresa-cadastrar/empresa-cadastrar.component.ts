import { Component, OnInit } from '@angular/core';
import { Empresa } from '../model/empresa.model';
import { EmpresaService } from '../empresa.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-empresa-cadastrar',
  templateUrl: './empresa-cadastrar.component.html',
  styleUrls: ['./empresa-cadastrar.component.scss']
})
export class EmpresaCadastrarComponent implements OnInit {

  constructor(private empresaService: EmpresaService,
              private toastr: ToastrService,
              private router: Router,
              private route: ActivatedRoute) { }

  empresa: Empresa = new Empresa();
  idString: string;
  idNumber: number;

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      if (params.get('id') != 'novo') {
        this.idString = params.get('id');
        this.idNumber = Number.parseInt(this.idString);
        this.empresaService.loadEmpresaBydId(this.idNumber).subscribe(
          empresa => {
            this.empresa = empresa;
          }
        )
      }
    });

  }

  submitEmpresa() {
    if(this.empresa.cnpjEmpresa == null || this.empresa.nomeEmpresa == null || this.empresa.razaoSocial == null) {
      this.toastr.error("Campos obrigatórios nulos");
    }
    else{
      this.empresaService.insertEmpresa(this.empresa).subscribe(
        () => {
          this.toastr.success("Cadastrado com sucesso!");           
        }
      ),
      setTimeout(() => {
        window.location.reload();
      }, 1000);  
    }
  }

}
