import { Component, OnInit } from '@angular/core';
import { EmpresaService } from '../empresa.service';
import { Empresa } from '../model/empresa.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-empresa-listar',
  templateUrl: './empresa-listar.component.html',
  styleUrls: ['./empresa-listar.component.scss']
})
export class EmpresaListarComponent implements OnInit {

  constructor(private empresaService: EmpresaService,
              private route: Router) { }
              
  empresas: Empresa[];
  pageAtual: number = 1;

  ngOnInit() {
    this.loadEmpresas();
  }

  loadEmpresas() {
    this.empresaService.loadEmpresa().subscribe(
      empresas => {
        this.empresas = empresas;
      }
    )
  }

  goToInserirEmpresa() {
    this.route.navigate(['/empresa/novo']);
  }

  alterarDados(empresa: Empresa) {
    this.route.navigate(['/empresa/'+empresa.guidEmpresa]);
  }

}
