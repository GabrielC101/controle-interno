import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-empresa-menu',
  templateUrl: './empresa-menu.component.html',
  styleUrls: ['./empresa-menu.component.scss']
})
export class EmpresaMenuComponent implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {
  }

  goToEmpresa() {
    this.route.navigate(['/empresa']);
  }

  goToDepartamento() {
    this.route.navigate(['/departamento'])
  }

}
