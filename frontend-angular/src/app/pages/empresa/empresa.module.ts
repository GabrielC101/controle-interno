import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmpresaListarComponent } from './empresa-listar/empresa-listar.component';
import { EmpresaCadastrarComponent } from './empresa-cadastrar/empresa-cadastrar.component';
import { EmpresaMenuComponent } from './empresa-menu/empresa-menu.component';
import { DepartamentoListarComponent } from './departamento-listar/departamento-listar.component';
import { DepartamentoCadastrarComponent } from './departamento-cadastrar/departamento-cadastrar.component';
import { FormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [EmpresaListarComponent, EmpresaCadastrarComponent, EmpresaMenuComponent, DepartamentoListarComponent, DepartamentoCadastrarComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgxMaskModule.forRoot(),
    NgxPaginationModule
  ]
})
export class EmpresaModule { }
