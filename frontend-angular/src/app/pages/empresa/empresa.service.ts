import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Empresa } from './model/empresa.model';

const urlApiEmpresa = 'http://localhost:8080/api/ti/empresa/';
const urlApiInserirEmpresa = 'http://localhost:8080/api/ti/empresa/';

@Injectable({
  providedIn: 'root'
})
export class EmpresaService {

  constructor(private http: HttpClient) { }

  loadEmpresa(): Observable<Empresa[]> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.get<Empresa[]>(urlApiEmpresa, options);
  }

  insertEmpresa(empresa: Empresa): Observable<Empresa> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.post<Empresa>(urlApiInserirEmpresa, empresa, options);
  }

  loadEmpresaBydId(id: number): Observable<Empresa> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.get<Empresa>(urlApiInserirEmpresa+id, options);
  }

}
