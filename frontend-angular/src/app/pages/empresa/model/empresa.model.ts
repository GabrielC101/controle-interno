export class Empresa {
    guidEmpresa: number;
    nomeEmpresa: string;
    razaoSocial: string;
    cnpjEmpresa: string;
    identificadorMatriz: number;
}