export class NotaFiscal {
    guidNota: number;
    nomeArquivo: string;
    conteudoArquivo: any;
    contentType: string;
    guidEquipamento: number;
    tokenIdentificacao: string;
    detalhesNota: string;
    guidEmpresa: number;
}