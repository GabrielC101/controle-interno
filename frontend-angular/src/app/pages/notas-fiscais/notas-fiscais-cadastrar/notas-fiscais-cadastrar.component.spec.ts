import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotasFiscaisCadastrarComponent } from './notas-fiscais-cadastrar.component';

describe('NotasFiscaisCadastrarComponent', () => {
  let component: NotasFiscaisCadastrarComponent;
  let fixture: ComponentFixture<NotasFiscaisCadastrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotasFiscaisCadastrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotasFiscaisCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
