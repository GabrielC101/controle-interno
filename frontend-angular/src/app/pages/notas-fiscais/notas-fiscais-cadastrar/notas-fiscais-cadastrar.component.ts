import { Component, OnInit } from '@angular/core';
import { EmpresaService } from '../../empresa/empresa.service';
import { Empresa } from '../../empresa/model/empresa.model';
import { NotaFiscal } from '../model/notas-fiscais.model';
import { NotasFiscaisService } from '../notas-fiscais.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-notas-fiscais-cadastrar',
  templateUrl: './notas-fiscais-cadastrar.component.html',
  styleUrls: ['./notas-fiscais-cadastrar.component.scss']
})
export class NotasFiscaisCadastrarComponent implements OnInit {

  constructor(private empresaService: EmpresaService,
              private notasFiscaisService: NotasFiscaisService,
              private toastr: ToastrService) { }

  empresas: Empresa[];
  nota: NotaFiscal = new NotaFiscal();
  selectedOption2: string;
  selectedFiles: FileList;
  currentFileUpload: File;
  uploadedImage: File;
  idNumber: number;

  ngOnInit() {
    this.loadEmpresa();
  }

  loadEmpresa() {
    this.empresaService.loadEmpresa().subscribe(
      empresas => {
        this.empresas = empresas;
      }
    )
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  submitNotaFiscal() {
    if(this.selectedFiles == undefined) {
      this.toastr.error('Selecione o arquivo');
    }
    else {
      this.idNumber = Number.parseInt(this.selectedOption2);
      this.currentFileUpload = this.selectedFiles.item(0)
      this.notasFiscaisService.salvar(this.currentFileUpload, this.idNumber, this.nota.detalhesNota).subscribe(
        () => {
          this.toastr.success('Cadastro realizado com sucesso!');
        }
      )
      setTimeout(() => {
        window.location.reload();
      }, 1000)
    }
  }

}
