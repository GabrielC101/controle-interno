import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotasFiscaisListarComponent } from './notas-fiscais-listar.component';

describe('NotasFiscaisListarComponent', () => {
  let component: NotasFiscaisListarComponent;
  let fixture: ComponentFixture<NotasFiscaisListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotasFiscaisListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotasFiscaisListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
