import { Component, OnInit } from '@angular/core';
import { NotaFiscal } from '../model/notas-fiscais.model';
import { NotasFiscaisService } from '../notas-fiscais.service';
import { Empresa } from '../../empresa/model/empresa.model';
import { EmpresaService } from '../../empresa/empresa.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notas-fiscais-listar',
  templateUrl: './notas-fiscais-listar.component.html',
  styleUrls: ['./notas-fiscais-listar.component.scss']
})
export class NotasFiscaisListarComponent implements OnInit {

  constructor(private notasFiscaisService: NotasFiscaisService,
              private empresaService: EmpresaService,
              private toastr: ToastrService,
              private router: Router) { }

  empresas: Empresa[];
  selectedOption2: string;
  notas: NotaFiscal[];
  _procurar: string;
  idNumber: number;
  pageAtual: number = 1;

  ngOnInit() {
    this.loadEmpresa();
  }

  loadEmpresa() {
    this.empresaService.loadEmpresa().subscribe(
      empresas => {
        this.empresas = empresas;
      }
    )
  }

  popularTableFiltro() {
    this.idNumber = Number.parseInt(this.selectedOption2);
    if(this.selectedOption2 == undefined) {
      this.toastr.error("Preencha o filtro 'Empresa'");
    }
    else {
      if(this._procurar != undefined) {
        if(this._procurar.trim().length >= 2) {
          this.notasFiscaisService.findByText(this._procurar).subscribe(
            notas => {
              this.notas = notas;
            }
          )
        }
        else {
          this.notasFiscaisService.loadByIdEmpresa(this.idNumber).subscribe(
            notas => {
              this.notas = notas;
            }
          )
        }
      }  
      else {
        this.notasFiscaisService.loadByIdEmpresa(this.idNumber).subscribe(
          notas => {
            this.notas = notas;
          }
        )
      }
    }
  }

  goToInserirNotaFiscal() {
    this.router.navigate(['/nfe-cadastrar/novo']);
  }

  deletarNota(nota: NotaFiscal) {
    this.notasFiscaisService.deleteNota(nota.guidNota).subscribe(
      () => {
        this.toastr.success('Registro deletado!')
      }
    )
    setTimeout(() => {
      window.location.reload();
    }, 1000)
  }

}
