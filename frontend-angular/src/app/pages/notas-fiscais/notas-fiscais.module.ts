import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotasFiscaisListarComponent } from './notas-fiscais-listar/notas-fiscais-listar.component';
import { NotasFiscaisCadastrarComponent } from './notas-fiscais-cadastrar/notas-fiscais-cadastrar.component';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [NotasFiscaisListarComponent, NotasFiscaisCadastrarComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule
  ]
})
export class NotasFiscaisModule { }
