import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NotaFiscal } from './model/notas-fiscais.model';

const urlApiFindByEmpresa = 'http://localhost:8080/api/ti/notafiscal/empresa/';
const urlApiSalvarArquivo = 'http://localhost:8080/api/ti/notafiscal/empresa/';
const urlApiFindText = 'http://localhost:8080/api/ti/notafiscal/find/';
const urlApiDelete = 'http://localhost:8080/api/ti/notafiscal/';

@Injectable({
  providedIn: 'root'
})
export class NotasFiscaisService {

  constructor(private http: HttpClient) { }

  loadByIdEmpresa(id: number): Observable<NotaFiscal[]> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    }
    return this.http.get<NotaFiscal[]>(urlApiFindByEmpresa+id, options);
  }

  salvar(data: File, id: number, tToken: string): Observable<any> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    let formData = new FormData();
    formData.append('arquivo', data, data.name);
    return this.http.post<any>
      (urlApiSalvarArquivo+id+'/detalhes/'+tToken, formData, options);
  }

  findByText(text: string): Observable<NotaFiscal[]> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.get<NotaFiscal[]>(urlApiFindText+text, options);
  }

  deleteNota(id: number) {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.delete<NotaFiscal>(urlApiDelete+id, options);
  }

}
