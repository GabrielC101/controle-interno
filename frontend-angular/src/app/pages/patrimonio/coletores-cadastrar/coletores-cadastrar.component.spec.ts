import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColetoresCadastrarComponent } from './coletores-cadastrar.component';

describe('ColetoresCadastrarComponent', () => {
  let component: ColetoresCadastrarComponent;
  let fixture: ComponentFixture<ColetoresCadastrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColetoresCadastrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColetoresCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
