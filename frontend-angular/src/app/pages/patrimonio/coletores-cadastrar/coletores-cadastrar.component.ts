import { Component, OnInit } from '@angular/core';
import { Empresa } from '../../empresa/model/empresa.model';
import { Departamento } from '../../empresa/model/departamento.model';
import { Coletor } from '../model/coletor.model';
import { ColetoresService } from '../coletores.service';
import { ToastrService } from 'ngx-toastr';
import { EmpresaService } from '../../empresa/empresa.service';
import { DepartamentoService } from '../../empresa/departamento.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-coletores-cadastrar',
  templateUrl: './coletores-cadastrar.component.html',
  styleUrls: ['./coletores-cadastrar.component.scss']
})
export class ColetoresCadastrarComponent implements OnInit {

  constructor(private coletorService: ColetoresService,
    private toastr: ToastrService,
    private empresaService: EmpresaService,
    private departamentoService: DepartamentoService,
    private route: ActivatedRoute,
    private myRoute: Router) { }

  coletor: Coletor = new Coletor();
  empresas: Empresa[];
  departamentos: Departamento[];
  selectedOption: string;
  selectedOption2: string;
  idDepartamento: number;
  idEmpresa: number;
  idString: string;
  idNumber: number;

  ngOnInit() {
    this.loadDepartamento();
    this.loadEmpresa();
    this.route.paramMap.subscribe(params => {
      if (params.get('id') != 'novo') {
        this.idString = params.get('id');
        this.idNumber = Number.parseInt(this.idString);
        this.coletorService.loadById(this.idNumber).subscribe(
          coletor => {
            this.coletor = coletor;
            this.selectedOption = coletor.guidDepartamento.toString();
            this.selectedOption2 = coletor.guidEmpresa.toString();
          }
        )
      }
    });
  }

  submitColetor() {
    if (this.selectedOption == undefined || this.selectedOption2 == undefined ||
      this.coletor.marcaColetor == undefined || this.coletor.modeloColetor == undefined
      || this.coletor.responsavelColetor == undefined) {
        this.toastr.error("Campos obrigatórios nulos");
    }
    else {
      this.idDepartamento = Number.parseInt(this.selectedOption);
      this.idEmpresa = Number.parseInt(this.selectedOption2);
      this.coletor.guidDepartamento = this.idDepartamento;
      this.coletor.guidEmpresa = this.idEmpresa;
      this.coletorService.saveColetor(this.coletor).subscribe(
        () => {
          this.toastr.success("Cadastrado com sucesso!");
        }
      );
      setTimeout(() => {
        window.location.reload();
      }, 1000)
    }
  }

  loadEmpresa() {
    this.empresaService.loadEmpresa().subscribe(
      empresa => {
        this.empresas = empresa;
      }
    )
  }

  loadDepartamento() {
    this.departamentoService.loadDepartamentos().subscribe(
      departamento => {
        this.departamentos = departamento;
      }
    )
  }

  goToManutencao(coletor: Coletor) {
    this.myRoute.navigate(['/manutencao-coletor/'+coletor.guidColetor]);
  }

}
