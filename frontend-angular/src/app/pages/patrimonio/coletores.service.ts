import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Coletor } from './model/coletor.model';

const urlApiColetor = 'http://localhost:8080/api/ti/coletores/';

@Injectable({
  providedIn: 'root'
})
export class ColetoresService {

  constructor(private http: HttpClient) { }

  loadColetoresByEmpresaAndDepartamento(id: number, iddep: number): Observable<Coletor[]> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.get<Coletor[]>(urlApiColetor+'empresa/'+id+'/departamento/'+iddep, options);
  }

  loadById(id: number): Observable<Coletor> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.get<Coletor>(urlApiColetor+id, options);
  }

  saveColetor(coletor: Coletor) {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.post<Coletor>(urlApiColetor, coletor, options);
  }

}
