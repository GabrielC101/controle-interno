import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColetoresComponent } from './coletores.component';

describe('ColetoresComponent', () => {
  let component: ColetoresComponent;
  let fixture: ComponentFixture<ColetoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColetoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColetoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
