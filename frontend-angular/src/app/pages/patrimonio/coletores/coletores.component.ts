import { Component, OnInit } from '@angular/core';
import { Empresa } from '../../empresa/model/empresa.model';
import { Departamento } from '../../empresa/model/departamento.model';
import { Computador } from '../model/computador.model';
import { DepartamentoService } from '../../empresa/departamento.service';
import { EmpresaService } from '../../empresa/empresa.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Coletor } from '../model/coletor.model';
import { ColetoresService } from '../coletores.service';

@Component({
  selector: 'app-coletores',
  templateUrl: './coletores.component.html',
  styleUrls: ['./coletores.component.scss']
})
export class ColetoresComponent implements OnInit {

  constructor(private empresaService: EmpresaService,
              private departamentoService: DepartamentoService,
              private route: Router,
              private toastr: ToastrService,
              private coletorService: ColetoresService) { }

  empresas: Empresa[];
  departamentos: Departamento[];
  selectedOption: string;
  selectedOption2: string;
  guiDepartamento: number;
  guidEmpresa: number;
  coletores: Coletor[];

  ngOnInit() {
    this.loadEmpresa();
    this.loadDepartamento();
  }

  loadEmpresa() {
    this.empresaService.loadEmpresa().subscribe(
      empresa => {
        this.empresas = empresa;
      }
    )
  }

  loadDepartamento() {
    this.departamentoService.loadDepartamentos().subscribe(
      departamento => {
        this.departamentos = departamento;
      }
    )
  }

  popularTableFiltro() {
    if(this.selectedOption == undefined || this.selectedOption2 == undefined) {
      this.toastr.error("Preencha o filtro");
    }
    else {
      this.guidEmpresa = Number.parseInt(this.selectedOption2);
      this.guiDepartamento = Number.parseInt(this.selectedOption);
      this.coletorService.loadColetoresByEmpresaAndDepartamento(this.guidEmpresa, this.guiDepartamento).subscribe(
        coletor => {
          this.coletores = coletor;
        }
      )
    }
  }

  goToInserirColetor() {
    this.route.navigate(['/coletores/novo']);
  }

  alterarDados(coletor: Coletor) {
    this.route.navigate(['/coletores/'+coletor.guidColetor]);
  }

}
