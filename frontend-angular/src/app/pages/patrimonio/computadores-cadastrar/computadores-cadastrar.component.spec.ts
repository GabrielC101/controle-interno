import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComputadoresCadastrarComponent } from './computadores-cadastrar.component';

describe('ComputadoresCadastrarComponent', () => {
  let component: ComputadoresCadastrarComponent;
  let fixture: ComponentFixture<ComputadoresCadastrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComputadoresCadastrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComputadoresCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
