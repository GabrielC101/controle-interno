import { Component, OnInit } from '@angular/core';
import { Computador } from '../model/computador.model';
import { ComputadoresService } from '../computadores.service';
import { ToastrService } from 'ngx-toastr';
import { Departamento } from '../../empresa/model/departamento.model';
import { Empresa } from '../../empresa/model/empresa.model';
import { EmpresaService } from '../../empresa/empresa.service';
import { DepartamentoService } from '../../empresa/departamento.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-computadores-cadastrar',
  templateUrl: './computadores-cadastrar.component.html',
  styleUrls: ['./computadores-cadastrar.component.scss']
})
export class ComputadoresCadastrarComponent implements OnInit {

  constructor(private computadorService: ComputadoresService,
              private toastr: ToastrService,
              private empresaService: EmpresaService,
              private departamentoService: DepartamentoService,
              private route: ActivatedRoute,
              private myRoute: Router) { }

  computador: Computador = new Computador();
  empresas: Empresa[];
  departamentos: Departamento[];
  selectedOption: string;
  selectedOption2: string;
  idDepartamento: number;
  idEmpresa: number;
  idString: string;
  idNumber: number;

  ngOnInit() {
    this.loadDepartamento();
    this.loadEmpresa();
    this.route.paramMap.subscribe(params => {
      if (params.get('id') != 'novo') {
        this.idString = params.get('id');
        this.idNumber = Number.parseInt(this.idString);
        this.computadorService.loadById(this.idNumber).subscribe(
          computador => {
            this.computador = computador;
            this.selectedOption = computador.departamentoComputador.toString();
            this.selectedOption2 = computador.guidEmpresa.toString();
          }
        )
      }
    });
  }

  submitComputador() {
    if (this.computador.modeloComputador == undefined
      || this.computador.ipv4Maquina == undefined
      || this.selectedOption == undefined || this.selectedOption2 == undefined) {
      this.toastr.error("Campos obrigatórios nulos");
    }
    else {
      this.idDepartamento = Number.parseInt(this.selectedOption);
      this.idEmpresa = Number.parseInt(this.selectedOption2);
      this.computador.guidEmpresa = this.idEmpresa;
      this.computador.departamentoComputador = this.idDepartamento;
      this.computadorService.insertComputador(this.computador).subscribe(
        () => {
          this.toastr.success("Cadastrado com sucesso!");
        }
      );
      setTimeout(() => {
        window.location.reload();
      }, 1000)
    }
  }

  loadEmpresa() {
    this.empresaService.loadEmpresa().subscribe(
      empresa => {
        this.empresas = empresa;
      }
    )
  }

  loadDepartamento() {
    this.departamentoService.loadDepartamentos().subscribe(
      departamento => {
        this.departamentos = departamento;
      }
    )
  }

  goToManutencao(computador: Computador) {
    this.myRoute.navigate(['/manutencao/'+computador.guidComputador]);
  }

}
