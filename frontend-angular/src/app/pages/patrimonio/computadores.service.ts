import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Computador } from './model/computador.model';

const urlApiListComputadores = 'http://localhost:8080/api/ti/computador/';
const urlApiComputadores = 'http://localhost:8080/api/ti/computador/empresa/';
const urlApiInserirComputadores = 'http://localhost:8080/api/ti/computador/';

@Injectable({
  providedIn: 'root'
})
export class ComputadoresService {

  constructor(private http: HttpClient) { }

  loadByEmpresa(id: number, idDep: number): Observable<Computador[]> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    }
    return this.http.get<Computador[]>(urlApiComputadores+id+'/departamento/'+idDep, options);
  }

  insertComputador(computador: Computador): Observable<Computador> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.post<Computador>(urlApiInserirComputadores, computador, options);
  }

  loadById(id: number): Observable<Computador> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.get<Computador>(urlApiListComputadores+id, options);
  }

}
