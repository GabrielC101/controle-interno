import { Component, OnInit } from '@angular/core';
import { EmpresaService } from '../../empresa/empresa.service';
import { Empresa } from '../../empresa/model/empresa.model';
import { Departamento } from '../../empresa/model/departamento.model';
import { DepartamentoService } from '../../empresa/departamento.service';
import { Computador } from '../model/computador.model';
import { ComputadoresService } from '../computadores.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-computadores',
  templateUrl: './computadores.component.html',
  styleUrls: ['./computadores.component.scss']
})
export class ComputadoresComponent implements OnInit {

  constructor(private empresaService: EmpresaService,
              private departamentoService: DepartamentoService,
              private computadorService: ComputadoresService,
              private route: Router,
              private toastr: ToastrService) { }

  empresas: Empresa[];
  departamentos: Departamento[];
  selectedOption: string;
  selectedOption2: string;
  guiDepartamento: number;
  guidEmpresa: number;
  computadores: Computador[];
  pageAtual: number;

  ngOnInit() {
    this.loadEmpresa();
    this.loadDepartamento();
  }

  loadEmpresa() {
    this.empresaService.loadEmpresa().subscribe(
      empresa => {
        this.empresas = empresa;
      }
    )
  }

  loadDepartamento() {
    this.departamentoService.loadDepartamentos().subscribe(
      departamento => {
        this.departamentos = departamento;
      }
    )
  }

  popularTableFiltro() {
    if(this.selectedOption == undefined || this.selectedOption2 == undefined) {
      this.toastr.error("Preencha o filtro");
    }
    else {
      this.guidEmpresa = Number.parseInt(this.selectedOption2);
      this.guiDepartamento = Number.parseInt(this.selectedOption);
      this.computadorService.loadByEmpresa(this.guidEmpresa, this.guiDepartamento).subscribe(
        computador => {
          this.computadores = computador;
        }
      )
    }
  }

  goToInserirComputador() {
    this.route.navigate(['/computadores/novo']);
  }

  alterarDados(computador: Computador) {
    this.route.navigate(['/computadores/'+computador.guidComputador]);
  }

}
