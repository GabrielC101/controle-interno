import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManutencaoCadastrarComponent } from './manutencao-cadastrar.component';

describe('ManutencaoCadastrarComponent', () => {
  let component: ManutencaoCadastrarComponent;
  let fixture: ComponentFixture<ManutencaoCadastrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManutencaoCadastrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManutencaoCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
