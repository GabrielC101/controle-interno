import { Component, OnInit } from '@angular/core';
import { Manutencao } from '../model/manutencao.model';
import { ManutencaoService } from '../manutencao.service';
import { NotasfiscaisService } from '../notasfiscais.service';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-manutencao-cadastrar',
  templateUrl: './manutencao-cadastrar.component.html',
  styleUrls: ['./manutencao-cadastrar.component.scss']
})
export class ManutencaoCadastrarComponent implements OnInit {

  constructor(private manutencaoService: ManutencaoService,
              private notasFiscaisService: NotasfiscaisService,
              private route: ActivatedRoute,
              private toastr: ToastrService) { }

  manutencao: Manutencao = new Manutencao();
  selectedFiles: FileList;
  currentFileUpload: File;
  uploadedImage: File;
  numeroId: number;
  idString: string;
  idNumber: number;
  possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890&";
  lengthOfCode = 40;
  tokenSecurity: string

  makeRandom(lengthOfCode: number, possible: string) {
    let text = "";
    for (let i = 0; i < lengthOfCode; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
      return text;
  }
  
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.idString = params.get('id');
      this.idNumber = Number.parseInt(this.idString);
    })
    this.tokenSecurity = this.makeRandom(this.lengthOfCode, this.possible);
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  submitManutencao() {
    if(this.manutencao.nomeManutencao == undefined || this.manutencao.detalhesManutencao == undefined) {
      this.toastr.error("Campos obrigatórios nulos");
    }
    if(this.selectedFiles == undefined) {
      this.manutencao.guidEquipamento = this.idNumber;
      this.manutencao.tokenIdentificacao = this.tokenSecurity;
      this.manutencaoService.saveManutencao(this.manutencao).subscribe(
        () => {

        }
      )
      this.toastr.success('Cadastrado com sucesso, sem nota!');
      setTimeout(() => {
        window.location.reload();
      },1000);
    }
    else {
      this.currentFileUpload = this.selectedFiles.item(0)
      this.notasFiscaisService.salvar(this.currentFileUpload, this.idNumber, this.tokenSecurity).subscribe(
        () => {
          this.notasFiscaisService.findByTokenIdentificacao(this.tokenSecurity).subscribe(
            nota => {
              this.manutencao.tokenIdentificacao = this.tokenSecurity;
              this.manutencao.guidNotaFiscal = nota.guidNota;
              this.manutencao.guidEquipamento = this.idNumber;
              this.manutencaoService.saveManutencao(this.manutencao).subscribe(
                () => {
      
                }
              )
            }
          );
        }
      );
      this.toastr.success('Cadastrado com sucesso!');
      setTimeout(() => {
        window.location.reload();
      },1000);
    }
  }

}
