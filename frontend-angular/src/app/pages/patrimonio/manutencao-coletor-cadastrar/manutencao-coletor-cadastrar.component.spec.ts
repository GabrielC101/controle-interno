import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManutencaoColetorCadastrarComponent } from './manutencao-coletor-cadastrar.component';

describe('ManutencaoColetorCadastrarComponent', () => {
  let component: ManutencaoColetorCadastrarComponent;
  let fixture: ComponentFixture<ManutencaoColetorCadastrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManutencaoColetorCadastrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManutencaoColetorCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
