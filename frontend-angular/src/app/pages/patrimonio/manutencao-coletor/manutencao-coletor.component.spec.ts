import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManutencaoColetorComponent } from './manutencao-coletor.component';

describe('ManutencaoColetorComponent', () => {
  let component: ManutencaoColetorComponent;
  let fixture: ComponentFixture<ManutencaoColetorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManutencaoColetorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManutencaoColetorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
