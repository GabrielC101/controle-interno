import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Manutencao } from './model/manutencao.model';
import { Observable } from 'rxjs';
import { ManutencaoColetor } from './model/manutencao-coletor.model';

const urlApiManutencao = 'http://localhost:8080/api/ti/manutencao/periferico/';
const urlApiSaveManutencao = 'http://localhost:8080/api/ti/manutencao/';
const urlApiManutencaoColetor = 'http://localhost:8080/api/ti/manutencao/coletor/periferico/';
const urlApiSaveManutencaoColetor = 'http://localhost:8080/api/ti/manutencao/coletor/';

@Injectable({
  providedIn: 'root'
})
export class ManutencaoService {

  constructor(private http: HttpClient) { }

  loadManutencaoPorPeriferico(id: number): Observable<Manutencao[]> {
    let options = {
      headers: new HttpHeaders().set('Authorization', 'Bearer '+localStorage.getItem('token'))
    };
    return this.http.get<Manutencao[]>(urlApiManutencao+id, options);
  }

  saveManutencao(manutencao: Manutencao): Observable<Manutencao> {
    let options = {
      headers: new HttpHeaders().set('Authorization', 'Bearer '+localStorage.getItem('token'))
    };
    return this.http.post<Manutencao>(urlApiSaveManutencao, manutencao, options);
  }

  deleteManutencao(id: number): Observable<any> {
    let options = {
      headers: new HttpHeaders().set('Authorization', 'Bearer '+localStorage.getItem('token'))
    };
    return this.http.delete<Manutencao>(urlApiSaveManutencao+id, options);
  }

  loadManutencaoPorPerifericoColetor(id: number): Observable<ManutencaoColetor[]> {
    let options = {
      headers: new HttpHeaders().set('Authorization', 'Bearer '+localStorage.getItem('token'))
    };
    return this.http.get<ManutencaoColetor[]>(urlApiManutencaoColetor+id, options);
  }

  saveManutencaoColetor(manutencao: ManutencaoColetor): Observable<ManutencaoColetor> {
    let options = {
      headers: new HttpHeaders().set('Authorization', 'Bearer '+localStorage.getItem('token'))
    };
    return this.http.post<ManutencaoColetor>(urlApiSaveManutencaoColetor, manutencao, options);
  }

  deleteManutencaoColetor(id: number): Observable<any> {
    let options = {
      headers: new HttpHeaders().set('Authorization', 'Bearer '+localStorage.getItem('token'))
    };
    return this.http.delete<Manutencao>(urlApiSaveManutencaoColetor+id, options);
  }

}
