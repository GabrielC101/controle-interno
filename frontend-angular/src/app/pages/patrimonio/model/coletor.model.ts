export class Coletor {
    guidColetor: number;
    responsavelColetor: string;
    modeloColetor: string;
    marcaColetor: string;
    serialIdentificador: string;
    guidEmpresa: number;
    guidDepartamento: number;
}