export class Computador {
    guidComputador: number;
    modeloComputador: string;
    departamentoComputador: number;
    dataFabricacao: Date;
    serialIdentificador: string;
    sistemaOperacional: string;
    hostMaquina: string;
    ipv4Maquina: string;
    guidUsuario: number;
    guidEmpresa: number;
    statusAtivo: number;
}