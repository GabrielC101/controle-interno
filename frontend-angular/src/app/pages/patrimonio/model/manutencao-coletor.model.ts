export class ManutencaoColetor {
    guidManutencaoColetor: number;
    nomeManutencao: string;
    precoManutencao: number;
    guidNotaFiscal: number;
    detalhesManutencao: string;
    guidEquipamento: number;
    guidEmpresa: number;
    tokenIdentificacao: string;
    dataManutencao = Date;
}