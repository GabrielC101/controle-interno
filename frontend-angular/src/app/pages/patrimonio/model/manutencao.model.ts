export class Manutencao {
    guidManutencao: number;
    nomeManutencao: string;
    precoManutencao: number;
    guidNotaFiscal: number;
    detalhesManutencao: string;
    guidEquipamento: number;
    guidEmpresa: number;
    tokenIdentificacao: string;
    dataManutencao = Date;
}