export class NotasFiscais {
    guidNota: number;
    nomeArquivo: string;
    conteudoArquivo: any;
    contentType: string;
    guidEquipamento: number;
    tokenIdentificacao: string;
}