import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NotasFiscais } from './model/notasfiscais.model';

const urlApiNotasFiscais = 'http://localhost:8080/api/ti/notafiscal/';
const urlApiNotasFiscaisFind = 'http://localhost:8080/api/ti/notafiscal/';
const urlApiGetToken = 'http://localhost:8080/api/ti/notafiscal/token/';

@Injectable({
  providedIn: 'root'
})
export class NotasfiscaisService {

  constructor(private http: HttpClient) { }

  salvar(data: File, id: number, tToken: string): Observable<any> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    let formData = new FormData();
    formData.append('arquivo', data, data.name);
    return this.http.post<any>
      (urlApiNotasFiscais+id+'/token/'+tToken, formData, options);
  }

  findByIdEquipamento(id: number): Observable<NotasFiscais> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.get<NotasFiscais>(urlApiNotasFiscaisFind+id, options);
  }

  deleteNotasFiscais(id: number): Observable<any> {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.delete<NotasFiscais>(urlApiNotasFiscais+id, options);
  }

  findByTokenIdentificacao(token: string) {
    let options = {
      headers: new HttpHeaders().set('Authorization', "Bearer "+localStorage.getItem('token'))
    };
    return this.http.get<NotasFiscais>(urlApiGetToken+token, options);
  }

}
