import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrincipalComponent } from './principal/principal.component';
import { ComputadoresComponent } from './computadores/computadores.component';
import { ColetoresComponent } from './coletores/coletores.component';
import { FormsModule } from '@angular/forms';
import { ComputadoresCadastrarComponent } from './computadores-cadastrar/computadores-cadastrar.component';
import { NgxMaskModule } from 'ngx-mask';
import { ColetoresCadastrarComponent } from './coletores-cadastrar/coletores-cadastrar.component';
import { ManutencaoComponent } from './manutencao/manutencao.component';
import { ManutencaoCadastrarComponent } from './manutencao-cadastrar/manutencao-cadastrar.component';
import { NgxCurrencyModule } from 'ngx-currency';
import { ManutencaoColetorComponent } from './manutencao-coletor/manutencao-coletor.component';
import { ManutencaoColetorCadastrarComponent } from './manutencao-coletor-cadastrar/manutencao-coletor-cadastrar.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [PrincipalComponent, ComputadoresComponent, ColetoresComponent, ComputadoresCadastrarComponent, ColetoresCadastrarComponent, ManutencaoComponent, ManutencaoCadastrarComponent, ManutencaoColetorComponent, ManutencaoColetorCadastrarComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgxMaskModule,
    NgxCurrencyModule,
    NgxPaginationModule
  ]
})
export class PatrimonioModule { }
