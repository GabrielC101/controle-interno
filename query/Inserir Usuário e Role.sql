-- SET USER ADMINISTRATOR
USE UtilitarioTI2020
INSERT INTO USERS(email, name, password, username, profissaoDesignada) --PASSWORD: 123456
VALUES('admin@admin.com', 'Administrador', '$2a$10$7z0sdVzybDe96oINMfnYKuD4rb1L2C0nLOf35P6eOe0eNYmxK2sfu', 'admin', 'Administrador do Sistema')
-- INSERT ROLES
INSERT INTO ROLES(name)
VALUES('ROLE_ADMIN')
INSERT INTO ROLES(name)
VALUES('ROLE_USER')
-- USER WITH ROLE
INSERT INTO user_roles(user_id, role_id)
VALUES(1,1)




